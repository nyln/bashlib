##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## We need compatibility until we know what shell we are using ##
test -z `which bash` && printf 'Bash required!\n' && exit 250                       ## Quit if Bash is not found on the system
BASH_USED=`ps -p $$ -o command | tail -1 | cut -d ' ' -f 1`
test `basename $BASH_USED` != "bash" && printf 'Bash must be used!\n' && exit 251   ## Quit if bash has not initialised this script
test ${BASH_VERSINFO:0:1} -lt 4 && printf 'Bash 4 required!\n' && exit 252          ## Quit if the version of Bash is too old
#################################################################

## Set up some general environment variables
declare -rx BASHLIB_NAME="$( basename "${BASH_SOURCE}" )"
declare -rx BASHLIB_DIR="$( dirname $( readlink -m "${BASH_SOURCE}" ) )"
declare -rx BASHLIB_VERSION="1.6.5.1"
declare -rx SELF_NAME="$( basename "${0}" )"
declare -rx SELF_DIR="$( dirname $( readlink -m "${0}" ) )"
declare -rx TRUE=1
declare -rx FALSE=0

[[ ${1} == "-v" ]] && { printf "%s\n" "${BASHLIB_VERSION}"; exit 0; }               ## Check if the version switch has been used and display
[[ ${-} == "*r*" ]] && { printf "Unrestricted shell required!\n"; exit 253; }       ## Quit if the shell used to call this script is restricted
[[ "${BASH_SOURCE}" == "${0}" ]] && { printf "File not sourced.\n"; exit 254; }     ## Quit if this script has not been sourced

## USAGE:       bashlib.load [LIBRARY]
## DESCRIPTION: Loads bashlib [LIBRARY] into memory
## REQUIRES:    dirname
bashlib.load() {
  [[ ! ${1} ]] && return 1
  declare MODULE
  for MODULE in ${@}; do
    [[ -f "${BASHLIB_DIR}/modules/${MODULE}.sh" ]] && source "${BASHLIB_DIR}/modules/${MODULE}.sh"
  done
  return 0
}

## USAGE:       bashlib.get_available_modules
## DESCRIPTION: Returns list of available bashlib modules (located in ${BASHLIB_DIR}/modules)
## REQUIRES:    basename, uniq
bashlib.get_available_modules() {
  declare MODULE MODULE_FILE MODULE_LIST
  for MODULE in ${BASHLIB_DIR}/modules/*.sh; do
    MODULE_FILE=$( basename ${MODULE} )
    [[ ${MODULE_FILE,,} =~ ([a-z]+)".sh" ]] && MODULE_LIST="${MODULE_LIST}${BASH_REMATCH[1]}\n"
  done
  printf "%s" "$( printf "%b" "${MODULE_LIST}" | uniq )"
  return 0
}

## USAGE:       bashlib.get_functions
## DESCRIPTION: Returns the currently loaded functions
## REQUIRES:    uniq
bashlib.get_functions() {
  declare FUNCTION FUNCTION_LIST
  for FUNCTION in $( declare -F ); do
    [[ ${FUNCTION} =~ ^(.*)"."(.*)$ ]] && FUNCTION_LIST="${FUNCTION_LIST}${FUNCTION}\n"
  done
  printf "%s\n" "$( printf "%b" "${FUNCTION_LIST}" | uniq )"
  return 0
}

## USAGE:       bashlib.get_modules
## DESCRIPTION: Returns the currently loaded modules
## REQUIRES:    uniq
bashlib.get_modules() {
  declare MODULE MODULE_LIST
  for MODULE in $( declare -F ); do
    [[ ${MODULE} =~ ^(.*)"."(.*)$ ]] && MODULE_LIST="${MODULE_LIST}${BASH_REMATCH[1]}\n"
  done
  printf "%s\n" "$( printf "%b" "${MODULE_LIST}" | uniq )"
  return 0
}

## USAGE:       bashlib.unload [LIBRARY]
## DESCRIPTION: Unloads bashlib [LIBRARY] from memory
## REQUIRES:    grep, dir
bashlib.unload() {
  [[ ! ${1} ]] && return 1
  declare MODULE FUNCTION
  for MODULE in ${@}; do
    for FUNCTION in $( grep "()" "${BASHLIB_DIR}/modules/${MODULE}.sh" ); do
      [[ ! ${FUNCTION} =~ ""^(.*)"()"(.*)$"" ]] && continue || unset -f "${BASH_REMATCH[1]}"
    done
  done
  return 0
}

## USAGE:       bashlib.validate_dependencies
## DESCRIPTION: Returns a list of external binary dependencies of the loaded modules and if those dependencies are met or not (1 for yes, 0 for no)
## REQUIRES:    grep, sort, uniq, sed
bashlib.validate_dependencies() {
  declare D
  for D in grep sort uniq sed; do
    [[ $( which ${D} ) ]] && continue || return 1
  done
  declare LOADED_MODULE LOADED_MODULES LOADED_MODULES_FILES LOADED_MODULES_REQUIRES LOADED_MODULES_REQUIRE REQUIRES_LIST
  LOADED_MODULES=$( bashlib.get_modules )
  for LOADED_MODULE in ${LOADED_MODULES}; do
    [[ ${LOADED_MODULE} == "bashlib" ]] && LOADED_MODULE="${BASHLIB_DIR}/${LOADED_MODULE}.sh" || LOADED_MODULE="${BASHLIB_DIR}/modules/${LOADED_MODULE}.sh"
    LOADED_MODULES_FILES="${LOADED_MODULES_FILES}${LOADED_MODULE} "
  done
  LOADED_MODULES_REQUIRES=$( grep -h "^## REQUIRES" ${LOADED_MODULES_FILES} | grep -v "\-\-\-" | sed 's/,//g;s/##.*:\t//g;s/ /\n/g;s/^.*REQUIRES://g' | sort | uniq )
  for LOADED_MODULE_REQUIRE in ${LOADED_MODULES_REQUIRES}; do
    [[ $( which ${LOADED_MODULE_REQUIRE} ) ]] && {
      REQUIRES_LIST="${REQUIRES_LIST}1:${LOADED_MODULE_REQUIRE}\n"
    } || {
      REQUIRES_LIST="${REQUIRES_LIST}0:${LOADED_MODULE_REQUIRE}\n"
    }
  done
  printf "%b" "${REQUIRES_LIST}"
  return 0
}

## USAGE:       bashlib.version
## DESCRIPTION: Returns the revision date of bashlib
## REQUIRES:    ---
bashlib.version() {
  printf "%s\n" "${BASHLIB_VERSION}"
  return 0
}
