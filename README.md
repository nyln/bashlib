# 0 - License
----------

MIT License

Copyright (c) 2022 Maisy Donachie

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


# 1 - Getting Started
----------

## 1.0 - Preface
----------
Bashlib was intended to help close the gap between Bash and many other scripting languages, by
providing functionality not natively supported in Bash, or in which the information was limited,
or in which the implimentation was lack luster.

Some of the main benefits of bashlib are it's simplified (and concise) array handling, string
manipulation and math(s) functions. Along with some more prototype functionality including
rudimentry objects via the class system.

Bashlib is still in active development and the current version is considered "Beta".

## 1.1 - Disclaimer
----------
None of the copyright holders included in this document have any responsibility for potential
loss of data, system stability / integrity caused by Bashlibs functions. All execution of Bashlib
functions are at the users own risk.

## 1.2 - Reporting Bugs
----------
All bugs can be reported directly to <bugs@maisy.io>
Please be as detailed as possibly and include the bashlib version number and any relevant source
code in your bug report.

## 1.3 - Quick Start
----------
To get up and running with Bashlib; simply source the main "bashlib.sh" file in your script. Then,
use the bashlib.load fuction to load the required modules (either main or extension modules).

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.load "array" "core" "string"```
```SWITCHES=( $( core.parse_switches ${## } ) )```
```for (( i=0; i < ( $( array.len "SWITCHES" ) / 2 ); i++ )); do```
```case ${SWITCHES[${i}]} in```
```("help") echo "Usage: ${0} --demo=[STRING]"; exit ;;```
```("demo") string.char_wrap "${SWITCHES[$(( ${i} + 1 ))]}" 10; echo; exit ;;```
```(**) echo "Invalid switch ${SWITCHES[${i}]}"; exit ;;```

```esac```
```done```

## 1.4 - Basic Functions
----------

### 1.4.0 - bashlib.load
----------
```bashlib.load "module" "module" "module" ...```

Sources all modules specified without requiring full paths (will find them relative to the
bashlib main file). You can specify as many modules as you like to include which saves using
multiple source lines.

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.load "core" "math" "system"```

### 1.4.1 - bashlib.get_available_modules
---------
```bashlib.get_available_modules```

Prints all the available bashlib module names.

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.get_available_modules```

Will output:
-array
-class
-conversion
-core
-database
-date
-file
-http
-interface
-math
-net
-string
-system

### 1.4.2 - bashlib.get_functions
----------
```bashlib.get_functions```

Prints all the currently loaded bashlib functions.

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.get_functions```

Will output:
-bashlib.get_functions
-bashlib.get_modules
-bashlib.load
-bashlib.unload
-bashlib.version

### 1.4.3 - bashlib.get_modules
----------
```baslib.get_modules```

Prints all the currently loaded bashlib module names.

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.load "core" "math" "system"```
```bashlib.get_modules```

Will output:
-bashlib
-core
-math

### 1.4.4 - bashlib.unload
----------
```bashlib.unload "module" "module" "module" ...```

Unloads specified modules from memory. Destroys all functions, but does not affect any global
variables that may have been set by those functions.

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.load "core" "math" "system"```
```bashlib.unload "math" "system"```
```bashlib.get_modules```

Will output:
-core

### 1.4.5 - bashlib.validate_dependencies
----------
```bashlib.validate_dependencies```

Returns a list of all dependencies required by the loaded modules and if the dependency is met or not (1 for yes, 0 for no)

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.load "core"```
```bashlib.validate_dependencies```

Will output (something like):
-1:basename
-1:dir
-1:dirname
-1:grep
-1:rm
-1:sed
-1:sort
-1:touch
-1:uniq

### 1.4.6 - bashlib.version
----------
```bashlib.version```

Returns the current version of bashlib.

Example:
```#!/bin/bash```
```source ### /bin/bashlib/bashlib.sh```
```bashlib.version```

Will output (something like):
-v1.6.1


# 2 - Module / Function Reference
----------

## 2.0 - array
----------
Bashlib does not use or support word keys in arrays; Bash's implimentation is lacking and requires more upkeep than it's worth at this point.

### 2.0.0 - array.amend
----------
**array.amend [ARRAY_NAME] [INDEX] [VALUE]**

Allows you to modify an existing array item with a new value.

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.amend "array_A" "2" "item3_replacement"```

Result:
``` array_A=( "item1" "item2" "item3_replacement" )```

### 2.0.1 - array.append
----------
**array.append [ARRAY_NAME] [VALUES]**

Appends any number of values to an array. 

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.append "array_A" "item4" "item5" "item6"```

Result:
``` array_A=( "item1" "item2" "item3" "item4" "item5" )```

### 2.0.2 - array.concate
----------
**array.concate [NEW_ARRAY_NAME] [OLD_ARRAY_NAME_A] [OLD_ARRAY_NAME_B]**

Appends all values from an array to another, and creates a new array leaving both original arrays in tact.

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.create "array_B" "item4" "item5" "item6"```
``` array.concate "array_C" "array_A" "array_B"```

Result:
``` array_C=( "item1" "item2" "item3" "item4" "item5" "item6" )```

### 2.0.3 - array.copy
----------
**array.copy [NEW_ARRAY] [OLD_ARRAY]**

Copies one array to another (if the new array already exists, all items up to the number of items in the old array will be replaced)

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.create "array_B" "item4" "item5" "item6"```
``` array.copy "array_A" "array_B"```

Result:
``` array_A=( "item4" "item5" "item6" )```

### 2.0.4 - array.create
----------
**array.create [ARRAY_NAME] [VALUES]**

Creates a new array with values. Because of how Bash handles arrays, at least one value must be present for the array to get created.

Example:
``` array.create "array_A" "item1" "item2" "item3"```

Result:
``` array_A=( "item1" "item2" "item3" )```

### 2.0.5 - array.destroy
----------
**array.destroy [ARRAY_NAME]**

Removes all array items from memory.

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.destroy "array_A"```

Result:
``` array_A=()```

### 2.0.6 - array.find
----------
**array.find [ARRAY_NAME] [VALUE]**

Looks through an array for a specific value and returns the key id of matching entries. Matching is case sensitive and **not** fuzzy in this instance.


Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.find "array_A" "item2"```

Result:
``` 1```

### 2.0.7 - array.find_ilike
----------
**array.find_ilike [ARRAY_NAME] [SEARCH]**

Looks through an array for a specific value and returns the key id of matching entries. Matching is **not** case sensitive and fuzzy in this instance.


Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.find_ilike "array_A" "ITEM"```

Result:
``` 0 1 2```

### 2.0.8 - array.find_like
----------
**array.find_like [ARRAY_NAME] [SEARCH]**

Looks through an array for a specific value and returns the key id of matching entries. Matching is case sensitive and fuzzy in this instance.

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.find_ilike "array_A" "3"```

Result:
``` 2```

### 2.0.9- array.ifind
----------
**array.ifind [ARRAY_NAME] [VALUE]**

Looks through an array for a specific value and returns the key id of matching entries. Matching is **not** case sensitive and **not** fuzzy in this instance.


Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.ifind "array_A" "ITEM1"```

Result:
``` 0```

### 2.0.10 - array.len
----------
**array.len [ARRAY_NAME]**

Returns number of keys in an array

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.len "array_A"```

Result:
``` 3```

### 2.0.11 - array.list
----------
**array.list [ARRAY_NAME]**

Returns all values in an array as well as their key.

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.list "array_A"```

Result:
``` array_A[0]=item1```
``` array_A[1]=item2```
``` array_A[2]=item3```

### 2.0.12 - array.prepend
----------
**array.prepend [ARRAY_NAME] [VALUES]**

Much like array.append, except values are added to the start of an array. 

Example:
``` array.create "array_A" "item1" "item2" "item3"```
``` array.prepend "array_A" "item4" "item5"```

Result:
``` array_A=( "item4" "item5" "item1" "item2" "item3" )```

### 2.0.13 - array.sort
----------
**array.sort [ARRAY_NAME]**

Sorts an array in ascending alpha numerical order. As Bash arrays can only be 2D at this point, only takes the array_name as input.

Example:
``` array.create "array_A" "item3" "item1" "item2" "item4"```
``` array.sort "array_A"```

Result:
``` array_A=( "item1" "item2" "item3" "item4" )```

### 2.0.14 - array.sort_reverse
----------
**array.sort_revers [ARRAY_NAME]**

Sorts an array in decending alpha numerical order. As Bash arrays can only be 2D at this point, only takes the array_name as input.

Example:
``` array.create "array_A" "item3" "item1" "item2" "item4"```
``` array.sort_reverse "array_A"```

Result:
``` array_A=( "item4" "item3" "item2" "item1" )```

## 2.1 - class
----------
Bashlib classes are not as feature full as standard classes; they have limited scope and still rely heavily on functions.

They essentially create a new set of functions for each member class you create using their parent functions. So, members "bob" "jim" and "sue" all use the "User" class, and will therefore inherit all of the "User_" functions.
In terms of performance, this is slower than it would be to use one function with a case statement in it. However, in terms of code written, it does shorten the amount needed, fairly significantly.

Classes rely on two main functions "[CLASS_TYPE]__construct" and "[CLASS_TYPE]__destruct" to do a lot of the work. But, you can also create your own functions by writing them like a normal function.
Just with the name "[CLASS_TYPE]_[FUNCTION_NAME]" this will then assign that function to all member classes. So, "User_GetInfo" will then become "bob : GetInfo".

The other major feature of classes is the use of the fake "this" function. Which does a bit of hackery to work out who's calling it and what is being asked of it. You have to use operators when calling "this" which visually
appear like PHP's "this => XYZ" syntax. Using "this" allows you to remove any connection with direct member classes when performing actions.

As an example, the previous "GetInfo" command can be written as:

User_GetInfo () {
printf "Username: # s\n" $( this - username )
printf "Email: # s\n" $( this - email )
printf "Phone: # s\n" $( this - phone )
}

So the member class "bob" can be called with the GetInfo command and all of Bob's information can be retrieved. "this" has a few functions:

this .# . Displays the name of the current class
this : variable=value# : Sets variables
this - variable# : Prints a variable value
this ## FunctionName# ## runs a self named function (eg MyClass_FunctionName)

Obviously, "this" does not work outside the context of a class function, but there are substitutes instead:

sName="bob"
echo ${sName}# . Equivilant
${sName} : variable=value# : Equivilant
${sName} : variable# - Equivilant
${sName} : FunctionName# ## Equivilant


### 2.1.0 - class.destroy
----------
**class.destroy [CLASS_TYPE] [CLASS]**

Destroys a class.

Example:
``` User__construct () { this : name="no_name"; }```
``` class.create "User" "bob" { name="bob" }```
``` class.destroy "User" "bob"```

### 2.1.1 - class.new
----------
**class.new [NAME] { [VAR] [VAR]... }**

Creates a new 'class' -- only variables should be entered, both braces are required, but inner values are not.

Example:
``` User__construct () { this : name="no_name"; }```
``` class.new "User" "bob" { name="Bobby" }```
``` bob : name```
``` class.new "User" "jim" { }```
``` jim : name```

Result:
``` Bobby```
``` no_name```

### 2.1.2 - this
----------
**this [OPERATOR] [VAR=VAL]**

A pesudo operator for use in class functions.

Example:
``` User__construct () { this : name="no_name"; }```

## 2.2 - core
----------

### 2.2.0 - core.include
----------

### 2.2.1 - core.log
----------

### 2.2.2 - core.lock_create
----------

### 2.2.3 - core.lock_check
----------

### 2.2.4 - core.lock_remove
----------

### 2.2.5 - core.parse_switches
----------

### 2.2.6 - core.pid
----------

### 2.2.7 - core.pid_sub
----------

### 2.2.8- core.read_pipe
----------

### 2.2.9 - core.repeat
----------

### 2.2.10 - core.require
----------

### 2.2.11 - core.soft_lock_create
----------

### 2.2.12 - core.soft_lock_check
----------

### 2.2.13 - core.soft_lock_remove
----------

### 2.2.14 - core.subshell
----------

### 2.2.15 - core.time
----------

### 2.2.16 - core.quit
----------

### 2.2.17 - core.write
----------

## 2.3 - database
----------

### 2.3.0 - database.mysql_create
----------

### 2.3.1 - database.mysql_delete
----------

### 2.3.2 - database.mysql_query
----------

### 2.3.3 - database.mysql_user_assign
----------

### 2.3.4 - database.mysql_user_create
----------

### 2.3.5 - database.mysql_user_delete
----------

### 2.3.6 - database.sqlite_query
----------

## 2.4 - date
----------

### 2.4.0 - date.diff
----------

### 2.4.1 - date.days
----------

### 2.4.2 - date.get_date
----------

### 2.4.3 - date.get_date_time
----------

### 2.4.4 - date.get_seconds
----------

### 2.4.5 - date.get_time
----------

### 2.4.6 - date.is_leap_year
----------

### 2.4.7 - date.months
----------

### 2.4.8 - date.weeks
----------

### 2.4.9 - date.years
----------

## 2.5 - file
----------

### 2.5.0 - file.encrypt_aes
----------

### 2.5.1 - file.decrypt_aes
----------

### 2.5.2 - file.cat
----------

### 2.5.3 - file.dechaff
----------

### 2.5.4 - file.diff
----------

### 2.5.5 - file.lines
----------

### 2.5.6 - file.md5
----------

### 2.5.7 - file.parse_cfg
----------

### 2.5.8 - file.parse_xml
----------

### 2.5.9 - file.perm_check
----------

### 2.5.10 - file.perm_get
----------

### 2.5.11 - file.perm_set
----------

### 2.5.12 - file.remove
----------

### 2.5.13 - file.sha1
----------

### 2.5.14 - file.sock
----------

### 2.5.15 - file.tar_gz_pack
----------

### 2.5.16 - file.tar_gz_unpack
----------

### 2.5.17 - file.touch
----------

## 2.6 - http
----------

### 2.6.0 - http.content_init
----------

### 2.6.1 - http.cookie_data
----------

### 2.6.2 - http.cookie_delete
----------

### 2.6.3 - http.cookie_set
----------

### 2.6.4 - http.cookie_set_session
----------

### 2.6.5 - http.cookie_requests
----------

### 2.6.6 - http.decode
----------

### 2.6.7 - http.get_data
----------

### 2.6.8 - http.get_domain
----------

### 2.6.9 - http.get_page
----------

### 2.6.10 - http.get_port
----------

### 2.6.11 - http.get_requests
----------

### 2.6.12 - http.get_web_server
----------

### 2.6.13 - http.get_url
----------

### 2.6.14 - http.header
----------

### 2.6.15 - http.init
----------

### 2.6.16 - http.post_data
----------

### 2.6.17 - http.post_init
----------

### 2.6.18 - http.post_requests
----------

### 2.6.19 - http.redirect
----------

### 2.6.20 - http.server_info
----------

## 2.7 - math
----------

### 2.7.0 - math.bin2dec
----------

### 2.7.1 - math.bin2hex
----------

### 2.7.2 - math.cal
----------

### 2.7.3 - math.ceil
----------

### 2.7.4- math.choose
----------

### 2.7.5 - math.dec2bin
----------

### 2.7.6 - math.dec2hex
----------

### 2.7.7 - math.deg2rad
----------

### 2.7.8- math.fact
----------

### 2.7.9 - math.floor
----------

### 2.7.10 - math.hex2bin
----------

### 2.7.11 - math.hex2dec
----------

### 2.7.12 - math.hypot
----------

### 2.7.13 - math.is_int
----------

### 2.7.14 - math.is_float
----------

### 2.7.15 - math.max
----------

### 2.7.16 - math.min
----------

### 2.7.17 - math.pi
----------

### 2.7.18 - math.rad2deg
----------

### 2.7.19 - math.rand
----------

### 2.7.20 - math.round
----------

## 2.8 - string
----------

### 2.8.0 - string.char_wrap
----------

### 2.8.1 - string.crop
----------

### 2.8.2 - string.len
----------

### 2.8.3 - string.len_words
----------

### 2.8.4 - string.lower
----------

### 2.8.5 - string.lower_first
----------

### 2.8.6 - string.lower_words
----------

### 2.8.7 - string.ltrim
----------

### 2.8.8 - string.md5
----------

### 2.8.9- string.random
----------

### 2.8.10 - string.replace
----------

### 2.8.11 - string.reverse
----------

### 2.8.12 - string.rtrim
----------

### 2.8.13 - string.sha1
----------

### 2.8.14 - string.strpos
----------

### 2.8.15 - string.strrchr
----------

### 2.8.16 - string.substr
----------

### 2.8.17 - string.trim
----------

### 2.8.18 - string.upper
----------

### 2.8.19 - string.upper_first
----------

### 2.8.20 - string.upper_words
----------

### 2.8.21 - string.word_wrap
----------


# 3 - Extensions
----------

## 3.0 - conversion
----------
The conversion module contains many useful functions for converting different measurement types.
Temperature, area, length, weight and more. The primary focus is on converting units that cannot
be equated easily; for example miliimeters to meters is a standrd multiplication, however,
converting miliimeters to inches is not. As such, not all units in a standard are included and
only a base minimum is (eg, milimeters are includes, centimeters and larger, and micrometer and
smaller are not included).

## 3.1 - interface
----------
The interface module contains several functions for displaying more user friendly interfaces to
the end user. Two types of navigatable menu are included as well as a number of loading bars
without relying on external applications / modules (like dialog or ncurses).

## 3.2 - net
----------
The net modules contains a handful of network related functions; getting IP values and sending
emails.

## 3.3 - system
----------
The system module contains functions for retreiving system information, from RAM and CPU usage to
checking for a running process and checking if the user invoking the script is root.


# 4 - Creating Your Own Extention
----------
As bashlib does not hard code any modules; it's easy enough to create shell scripts and include
them in the /extensions directory. While it is also possible to run fully independant scripts this
way, it's advisable not too.

## 4.0 - General Layout
----------
The final layout of your module is entirely up to you. However, it's advisable that you
follow a similar pattern to the main release / included extensions in function naming and
grouping.

It is strongly suggested that you mark the module file with an identification line that
distinguishes that it is not an officially supported module. Eg:

```## ---------- EXTENSION MODULE ----------```
```## This module is not offically supported```

All functions are in alpha numerical order. Each module contains only functions starting with
the name of module. For example:

string.sh
```string.substr ()```
```string.trim ()```

math.sh
```math.ceil ()```
```math.floor ()```

This allows for easy identification when reading the source of scripts.

All functions include three comment lines above the function which describes, breifly, the
purpose of the function, how to use it and what external applications it relies on. Bash builtins
are not included unless they require a special built in not included in Bash 4.x. For example:

string.sh
```## USAGE: string.upper [STRING]```
```# DESCRIPTION: Convert [STRING] to uppercase```
```## REQUIRES: ---```
```string.upper() {```

Three dashes mark that there are no external requirements for this function.

## 4.1 - Inclusion in the main Bashlib release
----------
If you wish for your extension module to be included in the main Bashlib release, please email it
to contrib## maisy.dev.

## 4.2 - Overriding Main Functions
----------
You can override any of the main functions by declaring the same functions names. It's advisable
that if you are overriding core functions, that you mark the filename, eg:

```core.ext.sh```

It is also suggested that you make it clear in the module file that it will override specific
core features.
