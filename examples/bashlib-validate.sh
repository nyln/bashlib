#!/bin/bash

source ../bashlib.sh
printf "Load array, class, core\n"
bashlib.load array class core
printf "Show loaded modules:\n"
bashlib.get_modules
printf "Validate loaded module dependencies:\n"
bashlib.validate_dependencies
printf "Unload array, class, core\n"
bashlib.unload array class core
printf "Get avaliable functions:\n"
bashlib.get_functions