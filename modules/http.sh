##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  http.content_init [LANGUAGE]
## DESCRIPTION:  Sends final page header with content-language identification
## REQUIRES:  ---
http.content_init() {
  [[ ! ${1} ]] && { printf "content-language: en\n\n"; return 0; } || { printf "content-language: %s\n\n" "${1}"; return 0; }
}

## USAGE:  http.cookie_data [COOKIE_NAME]
## DESCRIPTION:  Returns specific cookie data
## REQUIRES:  ---
http.cookie_data() {
  [[ ! ${1} || ! ${HTTP_COOKIE} ]] && return 1
  declare IFS
  IFS=";"
  declare -a q=( ${HTTP_COOKIE} )
  declare s
  for s in ${q[@]}; do
    IFS="="
    declare -a v=( ${s} )
    [[ "${v[0]}" == "${1}" || "${v[0]}" == " ${1}" ]] && break || continue
  done
  printf "%s" "${v[1]}"
  return 0
}

## USAGE:  http.cookie_delete [COOKIE_NAME]
## DESCRIPTION:  Blanks the cookie [COOKIE_NAME] which effectively destroys it
## REQUIRES:  ---
http.cookie_delete() {
  [[ ! ${1} ]] && return 1 || { printf "set-cookie: %s=\n" "${1}"; return 0; }
}

## USAGE:  http.cookie_set [COOKIE_NAME] [SECONDS_EXPIRY] [COOKIE_VALUE]
## DESCRIPTION:  Sets [COOKIE_NAME] with [COOKIE_VALUE] which expires after [SECONDS_EXPIRY] seconds.
## REQUIRES:  date
http.cookie_set() {
  [[ ! ${1} || ! ${2} || ! ${3} ]] && return 1
  printf "set-cookie: %s=%s; Expires=%s\n" "${1}" "${3}" "$( date "+%a, %d %b %Y %H:%M:S %Z" --date="+${2}seconds" )"
  return 0
}

## USAGE:  http.cookie_set_session [COOKIE_NAME] [COOKIE_VALUE]
## DESCRIPTION:  Sets [COOKIE_NAME] with [COOKIE_VALUE] which expires at the end of the browser session
## REQUIRES:  ---
http.cookie_set_session() {
  [[ ! ${1} || ! ${2} ]] && return 1 || { printf "set-cookie: %s=%s\n" "${1}" "${2}"; return 0; }
}

## USAGE:  http.cookie_requests
## DESCRIPTION:  Returns cookie request data
## REQUIRES:  ---
http.cookie_requests() {
  [[ ! ${HTTP_COOKIE} ]] && return 1
  declare IFS
  IFS=";"
  declare -a q=( ${HTTP_COOKIE} )
  declare s
  for s in ${q[@]}; do
    IFS="="
    declare -a v=( ${s} )
    [[ ${r} ]] && r="${r}${v[0]} ${v[1]}" || r="${v[0]} ${v[1]}"
  done
  printf "%s" "${r}"
  return 0
}

## USAGE:  http.decode [ESCAPED_STRING]
## DESCRIPTION:  Returns converted unicode escaped [ESCAPED_STRING]
## REQUIRES:  ---
http.decode() {
  declare r
  printf -v r '%b' "${1//%/\\x}"
  printf "%s" "${r}"
  return 0
}

## USAGE:  http.get_data [GET_NAME]
## DESCRIPTION:  Returns specific GET data
## REQUIRES:  ---
http.get_data() {
  [[ ! ${1} || ! ${QUERY_STRING} ]] && return 1
  declare IFS
  IFS="&"
  declare -a q=( ${QUERY_STRING} )
  declare s r
  for s in ${q[@]}; do
    IFS="="
    declare -a v=( ${s} )
    IFS="+"
    declare -a p=( ${v[1]} )
    [[ "${v[0]}" == "${1}" ]] && r="${p[@]}" || continue
  done
  printf "%s" "${r}"
  return 0
}


## USAGE:  http.get_domain
## DESCRIPTION:  Returns domain the page is on
## REQUIRES:  ---
http.get_domain() {
  [[ ! ${SERVER_NAME} ]] && return 1 || { printf "%s" "${SERVER_NAME}"; return 0; }
}

## USAGE:  http.get_page
## DESCRIPTION:  Returns the current page
## REQUIRES:  ---
http.get_page() {
  [[ ! ${SCRIPT_NAME} ]] && return 1 || { printf "%s" "${SCRIPT_NAME}"; return 0; }
}

## USAGE:  http.get_port
## DESCRIPTION:  Returns the current port being used
## REQUIRES:  ---
http.get_port() {
  [[ ! ${SERVER_PORT} ]] && return 1 || { printf "%s" "${SERVER_PORT}"; return 0; }
}

## USAGE:  http.get_requests
## DESCRIPTION:  Returns GET request data
## REQUIRES:  ---
http.get_requests() {
  [[ ! ${QUERY_STRING} ]] && return 1
  declare IFS
  IFS="&"
  declare -a q=( ${QUERY_STRING} )
  declare s r
  for s in ${q[@]}; do
    IFS="="
    declare -a v=( ${s} )
    [[ ${r} ]] && r="${r} ${v[0]} ${v[1]}" || r="${v[0]} ${v[1]}"
  done
  printf "%s" "${r}"
  return 0
}

## USAGE:  http.get_web_server
## DESCRIPTION:  Returns the web server the page is running from
## REQUIRES:  ---
http.get_web_server() {
  [[ ! ${SERVER_SOFTWARE} ]] && return 1 || { printf "%s" "${SERVER_SOFTWARE}"; return 0; }
}

## USAGE:  http.get_url
## DESCRIPTION:  Returns the full url
## REQUIRES:  ---
http.get_url() {
  printf "%s%s" "$( http.get_domain )" "$( http.get_page )"
  return 0
}

## USAGE:  http.header [HEADER_NAME] [HEADER_VALUE]
## DESCRIPTION: Returns the [HEADER_NAME] with [HEADER_VALUE] in a HTTP header format
## REQUIRES:  ---
http.header() {
  [[ ! ${1} || ! ${2} ]] && return 1 || { printf "%s: %s\n" "${1}" "${2}"; return 0; }
}

## USAGE:  http.init
## DESCRIPTION:  Sends initial http header
## REQUIRES:  ---
http.init() {
  printf "content-type: text/html\n" 
  return 0
}

## USAGE:  http.post_data [POST_NAME]
## DESCRIPTION:  Returns specific post data
## REQUIRES:  ---
http.post_data() {
  [[ ! ${1} || ! ${POST_STRING} ]] && return 1
  declare IFS
  IFS="&"
  declare -a q=( ${POST_STRING} )
  declare s r
  for s in ${q[@]}; do
    IFS="="
    declare -a v=( ${s} )
    IFS="+"
    declare -a p=( ${v[1]} )
    [[ "${v[0]}" == "${1}" ]] && r="${p[@]}" || continue
  done
  printf "%s" "${r}"
  return 0
}


## USAGE:  http.post_init
## DESCRIPTION:  Required to correctly interpret POST data
## REQUIRES:  ---
http.post_init() {
  [[ "${CONTENT_TYPE}" != "application/x-www-form-urlencoded" ]] && return 1 
  [[ ! "${POST_STRING}" && "${REQUEST_METHOD}" == "POST" && "${CONTENT_LENGTH}" ]] && read -n ${CONTENT_LENGTH} POST_STRING
  return 0
}

## USAGE:  http.post_requests
## DESCRIPTION:  Returns post request data
## REQUIRES:  ---
http.post_requests() {
  [[ ! ${POST_STRING} ]] && return 1
  declare IFS
  IFS="&"
  declare -a q=( ${POST_STRING} )
  declare s r
  for s in ${q[@]}; do
    IFS="="
    declare -a v=( ${s} )
    [[ ${r} ]] && r="${r} ${v[0]} ${v[1]}" || r="${v[0]} ${v[1]}"
  done
  printf "%s" "${r}"
  return 0
}

## USAGE:  http.redirect [NEW_URL]
## DESCRIPTION:  Rediects page to [NEW_URL]
## REQUIRES:  ---
http.redirect() {
  [[ ! ${1} ]] && return 1 || { printf "Location: %s\n" "${1}"; return 0; }
}

## USAGE:  http.server_info
## DESCRIPTION:  Returns basic server information
## REQUIRES:  ---
http.server_info() {
  declare -i j
  declare -a i=( "AUTH_TYPE:" "CONTENT_TYPE:" "CONTENT_LENGTH:" "QUERY_STRING:" "POST_STRING:" "HTTP_COOKIE:"\
  "REMOTE_HOST:" "REMOTE_ADDR:" "REMOTE_USER:" "SERVER_SOFTWARE:" "SERVER_NAME:" "GATEWAY_INTERFACE:"\
         "SERVER_PROTOCOL:" "SERVER_PORT:" "REQUEST_METHOD:" )
  declare -a v=( "${AUTH_TYPE}" "${CONTENT_TYPE}" "${CONTENT_LENGTH}" "${QUERY_STRING}" "${POST_STRING}" "${HTTP_COOKIE}"\
  "${REMOTE_HOST}" "${REMOTE_ADDR}" "${REMOTE_USER}" "${SERVER_SOFTWARE}" "${SERVER_NAME}" "${GATEWAY_INTERFACE}"\
  "${SERVER_PROTOCOL}" "${SERVER_PORT}" "${REQUEST_METHOD}" )
  for (( j=0; j < ${#i[@]}; j++ )); do
    printf "%s %s;" "${i[${j}]}" "${v[${j}]}"
  done
  return 0
}
