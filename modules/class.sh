##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## -------- PROTOTYPE MODULE ---------
## This module is still in development

## USAGE:  class.destroy [CLASS_TYPE] [CLASS]
## DESCRIPTION:  Destroys class [CLASS] of type [CLASS_TYPE] along with all functions and variables. Runs [CLASS_TYPE]__destruct function if it exists
## REQUIRES:  grep
class.destroy() {
  declare CLASS_FUNC CLASS_FUNCS CLASS_VAR CLASS_VARS CLASS_DESC
  [[ ! ${1} || ! ${1} ]] && return 1
  CLASS_TYPE="${1}"
  CLASS_GROUP="${CLASS_TYPE}"
  CLASS_NAME="${2}"
  FUNC_NAME="${CLASS_NAME}"
  CLASS_FUNCS=$( declare -F | grep "${CLASS_NAME}" )
  CLASS_DESC=$( declare -F | grep "${CLASS_TYPE}__destruct" )
  CLASS_VARS=$( set | grep -e "^${CLASS_NAME}_." )
  [[ ${CLASS_DESC} =~ ^.+"${CLASS_TYPE}__destruct"$ ]] && eval "${CLASS_TYPE}__destruct"
  for CLASS_FUNC in ${CLASS_FUNCS}; do
    [[ ${CLASS_FUNC} =~ ^.+"${CLASS_NAME}_"(.*)$ ]] && unset ${CLASS_NAME}_${BASH_REMATCH[1]}
  done
  for CLASS_VAR in ${CLASS_VARS}; do
    [[ ${CLASS_VAR} =~ ^"${CLASS_NAME}_"(.*)"=".+$ ]] && unset ${CLASS_NAME}_${BASH_REMATCH[1]}
  done
  unset ${CLASS_NAME}
}

## USAGE:  class.new [NAME] { [VAR] [VAR]... }
## DESCRIPTION:  Prototype sudo-class function (generates a self named function to handle variable input and output). Also runs [NAME]__construct function if it exists
## REQUIRES:  grep
class.new() {
  declare CLASS_NAME CLASS_CONS FUNC_NAME CLASS_FUNCS
  [[ ! ${1} || ! ${2} || ${3} != "{" || $( eval "echo \${${#@}}" ) != "}" ]] && return 1
  CLASS_NAME="${1}"
  CLASS_GROUP="${CLASS_NAME}"
  FUNC_NAME="${2}"
  shift 3
  eval " ${FUNC_NAME} () {
      declare CLASS_NAME CLASS_VAR CLASS_FUNC VAR VAL
      CLASS_NAME=\"${FUNC_NAME}\"
      FUNC_NAME=\"${FUNC_NAME}\"
      CLASS_GROUP=\"${CLASS_NAME}\"
      [[ \${1} != \":\" ]] && return 1
      [[ ! \${2} ]] && return 2
      CLASS_FUNC=\$( declare -F | grep \"\${CLASS_GROUP}_\${2}\" )
      [[ \${CLASS_FUNC} ]] && { shift 2; \${CLASS_FUNC#declare -f} \${@}; return 0; }
      CLASS_VAR=\$( eval \"echo \\\${\${CLASS_NAME}_\${2}}\" )
      [[ \${2} =~ ^(.*)\"=\"(.*)$ || ! \${2} ]] && { VAR=\"\${BASH_REMATCH[1]}\"; VAL=\"\${BASH_REMATCH[2]}\"; } || { printf \"%s\" \"\${CLASS_VAR}\"; return 0; }
      declare -g ${FUNC_NAME}_\${VAR}=\"\${VAL}\"
  }"
  CLASS_CONS=$( declare -F | grep "${CLASS_NAME}__construct" )
  [[  ${CLASS_CONS} =~ ^.+"${CLASS_NAME}__construct"$ ]] && eval "${CLASS_NAME}__construct ${CLASS_FUNCS}"
  for x in "${@}"; do
    [[ "${x}" == "}" ]] && break ||  { [[ ${x} =~ ^(.*)=(.*)$ ]] && { [[ ${BASH_REMATCH[2]} ]] && eval "${FUNC_NAME}_${BASH_REMATCH[1]}='${BASH_REMATCH[2]}'"; } }
    CLASS_FUNCS="${CLASS_FUNCS} $( eval 'echo ${FUNC_NAME}_${x}' )"
  done
  return 0
}

## USAGE:  this [OPERATOR] [VAR=VAL]
## DESCRIPTION:  For use in setting and returning items relative to class functions. Use [:] to set a variable, [-] to return a variable, [@] to call a function and [.] to display the Class Name
## Requires:  ---
this() {
  (( ${#FUNCNAME[@]} <= 2 )) && return 1
  case ${1} in
    (":") eval "${FUNC_NAME}_${2}"; return 0 ;;
    ("-") eval "printf \"%s\" \"\${${FUNC_NAME}_${2}}\""; return 0 ;;
    ("@") eval "${CLASS_GROUP}_${2}"; return 0 ;;
    (".") printf "%s" "${FUNC_NAME}"; return 0 ;;
    (*) return 1 ;;
  esac
}
