##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- EXTENSION MODULE ----------
## This module is not offically supported

## USAGE:  interace.commit [STRING]
## DESCRIPTION:  Prints [STRING] followed by a Y/n/c confirmation message
## REQUIRES:  ---
interface.commit() {
  declare I
  printf "%s[(y)es/(n)o/(c)ancel] " "${@}"
  read -r -n 1 I
  case ${I,,} in
    ("y")  return 0 ;;
    ("n")  return 1 ;;
    ("c")  return 2 ;;
    (*)  commit "${@}" ;;
  esac
}

## USAGE:  interface.horizontal_menu_select [MENU_ITEMS]
## DESCRIPTION:  Prints a horizontal menu bar and allows you to select from [MENU_ITEMS]. Use $RETURN value to determine option. Left and right arrows navigate.
## REQUIRES:  ----
interface.horizontal_menu_select () {
  declare -a OPTIONS USERIN MENU
  declare -ig RETURN
  declare -i i
  for (( i=0; i < ${#@}; i++ )); do
    OPTIONS[${i}]="${@:$(( i + 1 )):1}"
  done
  for (( i=0; i < ${#OPTIONS[@]}; i++ )); do
    (( RETURN == i )) && MENU="${MENU}\e[0;30m\e[47m" || MENU="${MENU}\e[0m"
    MENU="${MENU}[${OPTIONS[${i}]}]"
    MENU="${MENU}\e[0m "
  done
  printf "\033[10000D%b\r\033[10000C" "${MENU}"
  read -r -s -n 1 USERIN
  case ${USERIN} in
    ("D") (( RETURN > 0 )) && (( RETURN-- )) ;;
    ("C") (( RETURN < ( ${#@} - 1 ) )) && (( RETURN++ )) ;;
    ("") [[ ! ${RETURN} ]] && RETURN=0; return 0 ;;
  esac
  interface.horizontal_menu_select "${@}"
}

## USAGE:  interace.key_buff_flush
## DESCRIPTION:  Dumps excess keypresses
## REQUIRES:  ---
interface.key_buff_flush() {
  declare G
  [[ ${?} -ne 0 ]] && return 1
  while (( ${?} == 0 )); do
    read -r -n 1 -t 0.1 G
  done
  return 0
}

## USAGE:  interface.menu_select [MENU_ITEMS]
## DESCRIPTION:  Prints a navigatable menu from [MENU_ITEMS] -- uses $RETURN variable for decisions
## REQUIRES:  ---
interface.menu_select() {
  declare l userin
  declare -i j k t
  declare -ig e RETURN
  [[ ! ${1} ]] && return 1
  j=0
  l=( ${@} )
  k=${#l[@]}
  clear
  [[ ! ${e} ]] && e=0
  [[ ${_mt} ]] && printf "${_mt}\n\n"
  while (( j < k )); do
    [[ ${e} == ${j} ]] && printf "\e[0;30m\e[47m" || printf "\e[0m"
    printf "[ "
    w=$(( 20 - ${#l[${j}]} ))
    (( ( w % 2 ) == 1 )) && printf " "
    w=$(( w / 2 ))
    t=0
    while (( t < w )); do
      printf " "
      (( t++ ))
    done
    printf "%s" "${l[${j}]}"
    t=0
    while (( t <= w )); do
      printf " "
      (( t++ ))
    done
    printf " ]\e[0m\n"
    (( j++ ))
  done
  printf "\n[UP/DOWN - PAGEUP/PAGEDOWN] Navigate Menu\n[ENTER/SPACE] Select Line\n[x] Quick Exit\n"
  read -r -s -n 1 userin
  case ${userin} in
    ("B"|"6")  (( e < ( k - 1 ) )) && (( e++ )); interface.menu_select "${l[@]}" ;;
    ("A"|"5")  (( e > 0 )) && (( e-- )); interface.menu_select "${l[@]}" ;;
    ("x")    RETURN=500; return 1 ;;
    ("")    RETURN=${e} ;;
    (*)    interface.menu_select ${l[@]} ;;
  esac
  return 0
}

## USAGE:  interface.menu_options [MENU_ITEMS]
## DESCRIPTION:  Prints a navigatable menu from [MENU_ITEMS] -- uses $o[] array for options
## REQUIRES:  ---
interface.menu_options() {
  declare l userin
  declare -i j k t
  declare -ig e RETURN
  [[ ! ${1} ]] && return 1
  j=0
  l=(${@})
  k=${#l[@]}
  clear
  [[ ! ${e} ]] && e=0
  [[ ${_mt} ]] && printf "${_mt}\n\n"
  while (( j < k )); do
    (( e == j )) && printf "\e[0;30m\e[47m" || printf "\e[0m"
    (( o[${j}] == 1 )) && printf "[x][ " || printf "[ ][ "
    w=$(( 20 - ${#l[${j}]} ))
    (( ( w % 2 ) == 1 )) && printf " "
    w=$(( w / 2 ))
    t=0
    while (( t < w )); do
      printf " "
      (( t++ ))
    done
    printf "%s" "${l[${j}]}"
    t=0
    while (( t <= w )); do
      printf " "
      (( t++ ))
    done
    printf " ]\e[0m\n"
    (( j++ ))
  done
  printf "\n[UP/DOWN - PAGEUP/PAGEDOWN] Navigate Menu\n[ENTER/SPACE] Select Line\n[x] Quick Exit\n"
  read -r -s -n 1 userin
  case ${userin} in
    ("B"|"6")  (( e < ( k - 1 ) )) && (( e++ )); interface.menu_options ${l[@]} ;;
    ("A"|"5")  (( e > 0 )) && (( e-- )); interface.menu_options ${l[@]} ;;
    ("x")    RETURN=500; return 1 ;;
    ("")    RETURN=${e} ;;
    (*)    interface.menu_options ${l[@]} ;;
  esac
  return 0
}

## USAGE:  interface.progress_bar [LENGTH]
## DESCRIPTION:  Prints a static progress bar of length [LENGTH] -- requires an external loop
## REQUIRES:  ---
interface.progress_bar() {
  declare -i j k l len
  [[ ! ${1} ]] && return 1
  len=${1}
  (( len == 0 )) && printf "\n" && return 1
  (( !i || i == ( len + 2 ) )) && i=0
  j=0
  l=$(( i + 1 ))
  while (( j < ( i - 1 ) )); do
    printf "-"
    (( j++ ))
  done
  (( i == 0 || i == ( len + 1 ) )) && printf ">" || printf ">>"
  while (( l <= len )); do
    printf "-"
    (( l++ ))
  done
  k=0
  while (( k <= len )); do
    printf "\b"
    (( k++ ))
  done
  (( i++ ))
  sleep 0.1
  return 0
}

## USAGE:  interface.shark_bar
## DESCRIPTION:  Loops a loading bar with a single moving element
## REQUIRES:  ---
## ----- THIS FUNCTION DOES NOT ACT LIKE OTHER INTERFACE LOADING PROMPTS. IT REQUIRES A SIGTERM OR SIGINT TO TERMINATE -----
interface.shark_bar () {
  trap "return 1" SIGINT SIGTERM
  declare -i BAR i j
  declare B SHARK
  BAR=10
  SHARK=">"
  for (( i=0; i <= ( BAR + 1 ); i++ )); do
    B="["
    for (( j=0; j < i; j++ )); do
      B="${B} "
    done
    (( i <= BAR )) && B="${B}${SHARK}"
    for (( j=i; j < BAR; j++ )); do
      B="${B} "
    done
    B="${B}]"
    printf "%s\r" "${B}"
    (( i > BAR )) && i=-1
    sleep 0.1
  done
  return 0
}

## USAGE:  interface.spinner
## DESCRIPTION:  Displays a loading spinner
## REQUIRES:  ---
interface.spinner() {
  declare -gi i
  declare s="\|/-"
  [[ ${i} == ${#s} ]] && i=0
  printf "\r${s:${i}:1}"
  (( i++ ))
  sleep 0.2
  return 0
}

## USAGE:  interface.spinner_inc 
## DESCRIPTION:  Displays a progressive loading spinner
## REQUIRES:  ---
interface.spinner_inc() {
  declare -gi i j
  declare s="\|/-"
  [[ ! ${j} ]] && j=0
  [[ ${j} == $(( ${#s} * 3 )) ]] && printf "*" && j=0
  [[ ${i} == ${#s} ]] && i=0
  printf "\b${s:${i}:1}"
  (( j++ ))
  (( i++ ))
  sleep 0.2
  return 0
}

## USAGE:  interface.ticker_print [STRING] [DELAY]
## DESCRIPTION:  Prints [STRING] with [DELAY] between each character
## REQUIRES:  ---
interface.ticker_print() {
  [[ ! ${1} || ! ${2} || ! ${2} =~ ^[.0-9]+$ ]] && return 1
  for (( i=0; i < "${#1}"; i++ )); do
    printf "%c" "${1:${i}:1}"
    sleep "${2}"
  done
  return 0
}

## USAGE:  interface.timer
## DESCRIPTION:  Displays a counting timer
## REQUIRE:  ---
interface.timer() {
  declare -ig ms s m h
  [[ ! ${ms} ]] && ms=0
  [[ ! ${s} ]] && s=0
  [[ ! ${m} ]] && m=0
  [[ ! ${h} ]] && h=0
  (( ms == 10 )) && ms="0" && (( s++ ))
  (( s == 60 )) && s="0" && (( m++ ))
  (( m == 60 )) && m="0" && (( h++ ))
  (( s < 10 )) && sR="0${s}" || sR="${s}"
  (( m < 10 )) && mR="0${m}" || mR="${m}"
  (( h < 10 )) && hR="0${h}" || hR="${h}"
  printf "%2.0f:%2.0f:%2.0f:%1.0f\r" "${hR}" "${mR}" "${sR}" "${ms}"
  (( ms++ ))
  sleep 0.1
  return 0
}

## USAGE:  interface.wait
## DESCRIPTION:  Waits for user to press any key
## REQUIRES:  ---
interface.wait() {
  declare p
  printf "Press any key to continue..."
  read -r -n 1 p
  printf "\n"
  return 0
}
