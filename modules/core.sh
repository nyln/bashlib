##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  core.include [FILES]
## DESCRIPTION:  Include extra files
## REQUIRES:  ---
core.include() {
  [[ ! ${1} ]] && return 1
  declare -i e=0
  declare s
  for s in ${@}; do
    [[ -f "${s}" ]] && source "${s}" || e=1
  done
  (( e == 1 )) && return 2
  return 0
}

## USAGE:  core.log [FILE] [LOG_MESSAGE]
## DESCRIPTION:  Logs [LOG_MESSAGE] to [FILE]
## REQUIRES:  ---
core.log() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare l="${1}"
  shift
  while [[ ${@} ]]; do
    printf "%b" "${1}" >> ${l}
    shift
  done
  printf "\n" >> ${l}
  return 0
}

## USAGE:  core.lock_create [NAME]
## DESCRIPTION:  Creates a lock file [NAME]
## REQUIRES:  touch
core.lock_create() {
  [[ ! ${1} ]] && return 1
  [[ -f "/tmp/${l}.lock" ]] && return 3
  touch "/tmp/${1}.lock"
  return 0
}

## USAGE:  core.lock_check [NAME]
## DESCRIPTION:  Checks if lock file [NAME] exists
## REQUIRES:  ---
core.lock_check() {
  [[ ! ${1} ]] && return 1
  [[ -f "/tmp/${1}.lock" ]] && return 0 || return 2
}

## USAGE:  core.lock_remove [NAME]
## DESCRIPTION:  Removes lock file [NAME]
## REQUIRES:  rm
core.lock_remove() {
  [[ ! ${1} ]] && return 1
  [[ ! -f "/tmp/${1}.lock" ]] && return 2
  rm "/tmp/${1}.lock"
  return 0
}

## USAGE:  core.parse_switches [INPUT]
## DESCRIPTION:  Returns values of [INPUT], read as command line switches as (VARIABLE VALUE VARIABLE VALUE)...
## REQUIRES:  ---
core.parse_switches() {
  [[ ! ${1} ]] && return 1
  declare -i i=0
  while [[ ${@} ]]; do
    if [[ "${1}" =~ ^[-][-](.*)[=](.*)$ ]]; then
      (( i > 0 )) && printf " "
      printf "%s %s" "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}"
      (( i++ ))
      shift
    elif [[ "${1}" =~ ^[-]([[:alnum:]])$ || "${1}" =~ ^[-][-]([[:alnum:]]*)$ ]]; then
      (( i > 0 )) && printf " "
      printf "%s %i" "${BASH_REMATCH[1]}" "1"
      (( i++ ))
      shift
    else
      (( i > 0 )) && printf " "
      printf "%s ERROR" "${1}"
      return 2
    fi
  done
  return 0
}

## USAGE:  core.pid
## DESCRIPTION:  Returns current PID
## REQUIRES:  ---
core.pid() {
  printf "%i" ${$}
  return 0
}

## USAGE:  core.pid_sub
## DESCRIPTION:  Returns current PID, if run in subshell will return subshell PID
## REQUIRES:  ---
core.pid_sub() {
  [[ ${$} == ${BASHPID} ]] && printf "%i"  ${$} || printf "%i" ${BASHPID}
  return 0
}

## USAGE:  core.read_pipe
## DESCRIPTION:  Returns input from a pipe
## REQUIRES:  ---
core.read_pipe() {
  declare i
  while read -t 0.1 data; do
    [[ ! ${i} ]] && i="${data}\n" || i="${i}${data}\n"
  done
  [[ ${i} ]] && { printf "%b" "${i}"; return 0; } || return 1
}

## USAGE:  core.repeat [TIMES] [COMMAND]
## DESCRIPTION:  Repeat [COMMAND] [TIMES] number of times
## REQUIRES:  ---
core.repeat() {
  [[ ! ${1} || ! ${2} ]] && return 1
  [[ ! ${1} =~ ^[0-9]+$ ]] && return 2
  declare r="${1}"
  declare -i i
  shift
  for (( i=0; i < ${r}; i++ )); do
    ${*}
  done
  return 0
}

## USAGE:  core.require [FILES]
## DESCRIPTION:  Sources [FILES] and aborts if any of [FILES] does not exist
## REQUIRES:  ---
core.require() {
  [[ ! ${1} ]] && return 1
  declare s
  for s in ${@}; do
    [[ ! -f "${s}" ]] && return 2
  done
  for s in ${@}; do
    source ${s}
  done
  return 0
}

## USAGE:  core.soft_lock_create
## DESCRIPTION:  Create a "soft lock" internal variable
## REQUIRES:  ---
core.soft_lock_create() {
  declare -g ENV_LOCK="1";
  [[ ${ENV_LOCK} == 1 ]] && return 0 || return 1
}

## USAGE:  core.soft_lock_check
## DESCRIPTION:  Checks if a "soft lock" is in place
## REQUIRES:  ---
core.soft_lock_check() {
  [[ ${ENV_LOCK} ]] && return 0 || return 1
}

## USAGE:  core.soft_lock_remove
## DESCRIPTION:  Removes a "soft lock"
## REQUIRES:  ---
core.soft_lock_remove() {
  [[ ${ENV_LOCK} ]] && unset ENV_LOCK && return 0 || return 1
}

## USAGE:  core.subshell
## DESCRIPTION:  Returns true or false if command is running in a subshell
## REQUIRES:  ---
core.subshell() {
  [[ ${$} == ${BASHPID} ]] && return 0 || return 1
}

## USAGE:  core.time [COMMAND]
## DESCRIPTION:  Measures the time to execute [COMMAND]
## REQUIRES:  ---
core.time() {
  [[ ! ${1} ]] && return 1
  declare s=$( date "+%s.%N" )
  ${*}
  declare e=$( date "+%s.%N" )
  declare t=$( printf "%f-%f\n" ${e} ${s} | bc )
  [[ ! ${s} || ! ${e} || ! ${t} ]] && return 2
  printf "%0.2f" "${t}"
  return 0
}

## USAGE:  core.write [STYLE] [COLOUR] [STRING]
## DESCRIPTION:  Print [STRING] in [COLOUR] and [STYLE]
## REQUIRES:  ---
core.write() {
  declare c cc
  declare -i s
  [[ ! ${1} ]] && return 1
  case ${1} in
    ("strong")   s=1 && shift ;;
    ("underline")  s=4 && shift ;;
    ("italic")  s=3 && shift ;;
    ("hidden")  s=2 && shift ;;
    ("normal")  s=0 && shift ;;
    (*)    s=0 ;;
  esac
  case ${1} in
    ("red")    c="\e[${s};31m" && cc="\e[0m" && shift ;;
    ("green")  c="\e[${s};32m" && cc="\e[0m" && shift ;;
    ("yellow")  c="\e[${s};33m" && cc="\e[0m" && shift ;;
    ("blue")  c="\e[${s};34m" && cc="\e[0m" && shift ;;
    ("purple")  c="\e[${s};35m" && cc="\e[0m" && shift ;;
    ("cyan")  c="\e[${s};36m" && cc="\e[0m" && shift ;;
    ("white")  c="\e[${s};37m" && cc="\e[0m" && shift ;;
    ("black")  c="\e[${s};30m" && cc="\e[0m" && shift ;;
    (*)    c="" && cc="" ;;
  esac
  printf "${c}${@}${cc}"
  return 0
}
