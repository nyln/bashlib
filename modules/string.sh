##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  string.char_wrap [STRING] [CUT]
## DESCRIPTION:  Wraps a string to [CUT] characters per line
## REQUIRES:  ---
string.char_wrap() {
  [[ ! ${1} || ! ${2} || ! ${2} =~ ^([0-9]+)$ ]] && return 1
  declare -i j=0 i
  for (( i=0; i <= ${#1}; i++ )); do
      (( j == ${2} )) && printf "\n" && j=0
    printf "%c" "${1:${i}:1}"
    (( j++ ))
  done
}

## USAGE:  string.crop [STRING] [LENGTH]
## DESCRIPTION:  Crops [STRING] to specified number of characters
## REQUIRES:  ---
string.crop() {
  [[ ! ${1} || ! ${2} ]] && return 1 || { printf "%s" "${1:0:${2}}"; return 0; }
}

## USAGE:  string.len [STRING]
## DESCRIPTION:  Returns number of characters in [STRING]
## REQUIRES:  ---
string.len() {
  [[ ! ${1} ]] && return 1 || { printf "%i" "${#1}"; return 0; }
}

## USAGE:  string.len_words [STRING]
## DESCRIPTION:  Returns number of words in [STRING]
## REQUIRES:  ---
string.len_words() {
  [[ ! ${1} ]] && return 1
  declare -i i=0
  declare x
  for x in ${1}; do
    (( i++ ))
  done
  printf "%i" ${i}
  return 0
}

## USAGE:  string.lower [STRING]
## DESCRIPTION:  Convert [STRING] to lowercase
## REQUIRES:  ---
string.lower() {
  [[ ! ${1} ]] && return 1 || { printf "%s" "${1,,}"; return 0; }
}

## USAGE:  string.lower_first [STRING]
## DESCRIPTION:  Convert first character of [STRING] to lowercase
## REQUIRES:  ---
string.lower_first() {
  [[ ! ${1} ]] && return 1 || { printf "%s" "${1,}"; return 0; }
}

## USAGE:  string.lower_words [STRING]
## DESCRIPTION:  Converts the first character of each word in [STRING] to lowercase
## REQUIRES:  ---
string.lower_words() {
  [[ ! ${1} ]] && return 1
  declare x
  for x in ${1}; do
    declare s="${s}${x,} "
  done
  printf "%s" "${s}"
  return 0
}

## USAGE:  string.ltrim [STRING]
## DESCRIPTION:  Trims whitespace from beginning of [STRING]
## REQUIRES:  ---
string.ltrim() {
  [[ ! ${1} ]] && return 1
  [[ ${1:0:1} =~ ^[[:space:]]$ ]] && printf "%s" "${1:1}" || printf "%s" "${1}"
  return 0
}

## USAGE:  string.md5 [STRING]
## DESCRIPTION:  Generate md5 hash of [STRING]
## REQUIRES:  openssl
string.md5() {
  [[ ! ${1} ]] && return 1 || { printf "%s" "${1}" | openssl md5 | cut -d " " -f 2; return 0; }
}

## USAGE:  string.random [LENGTH]
## DESCRIPTION:  Generate a pesudo-random string of [LENGTH]
## REQUIRES:  base64, tr, head
string.random() {
  [[ ! ${1} ]] || [[ ! ${1} =~ ^[0-9]+$ ]] && return 1
  printf "%s" "$( base64 /dev/urandom | tr -d '/+' | head -c ${1} )"
  return 0
}

## USAGE:  string.replace [STRING] [OLD_WORD] [NEW_WORD]
## DESCRIPTION:  Replaces [OLD_WORD] with [NEW_WORD] in [STRING]
## REQUIRES:  sed
string.replace() {
  [[ ! ${1} || ! ${2} || ! ${3} ]] && return 1 || { printf "%s" "${1}" | sed -e "s:${2}:${3}:g"; return 0; }
}

## USAGE:  string.reverse [STRING]
## DESCRIPTION:  Reverses characters in [STRING]
## REQUIRES:  ---
string.reverse() {
  [[ ! ${1} ]] && return 1
  declare s="${1}"
  declare -i i
  for (( i=0; i < ${#s}; i++ )); do
    declare v=$(( ( ${#s} - 1 ) - i ))
    declare t="${t}${s:${v}:1}"
  done
  printf "%s" "${t}"
  return 0
}

## USAGE:  string.rtrim [STRING]
## DESCRIPTION:  Trims whitespace from end of [STRING]
## REQUIRES:  ---
string.rtrim() {
  [[ ! ${1} ]] && return 1
  [[ ${1:$(( ${#1} - 1 )):1} =~ ^[[:space:]]$ ]] && printf "%s" "${1:0:$(( ${#1} - 1 ))}" || printf "%s" "${1}"
  return 0
}

## USAGE:  string.sha1 [STRING]
## DESCRIPTION:  Generate sha1 hash of [STRING]
## REQUIRES:  openssl
string.sha1() {
  [[ ! ${1} ]] && return 1 || { printf "%s" "${1}" | openssl sha1 | cut -d " " -f 2; return 0; }
}

## USAGE:  string.sprintf [PRINTF_PATTERN] [VAR] [VAR] [VAR]
## DESCRIPTION:  Reads input into variables [VAR] in order of [PRINTF_PATTERN]. Most patterns will match either way (printf is loose), but ints can't be strings for example.
## REQUIRES:  ---
string.sprintf () {
  declare -a TMP PAT=( ${1} )
  declare -i i
  shift && read -ra TMP || return 1
  (( ${#TMP[@]} < ${#@} )) && return 1
  for (( i=1; i <= ${#@}; i++ )); do
    eval "${@:${i}:1}=${TMP[$(( i - 1 ))]}"
    eval "printf '${PAT[$(( i - 1 ))]}' \"\$${@:${i}:1}\" &>/dev/null"
    (( ${?} > 0 )) && unset ${@:${i}:1} && return 2
  done
  return 0
}

## USAGE:  strpos [HAYSTACK] [NEEDLE] [OFFSET]
## DESCRIPTION:  Finds postion of the first occurence of a substring in a string starting at position [OFFSET]. [OFFSET] is optional
## REQUIRES:  ---
string.strpos() {
  declare -i i O
  [[ ! ${1} || ! ${2} ]] || [[ ${3} && ! ${3} =~ ^[0-9]+$ ]] && return 1
  [[ ! ${3} && ${1} != *${2}* ]] && { printf "%i" "-1"; return 1; }
  [[ ${3} && ${1:${3}} != *${2}* ]] && { printf "%i" "-1"; return 1; }
  [[ ${3} ]] && O="${3}" || O="0"
  for (( i=$(( 0 + O )); i < ${#1}; i++ )); do
    [[ ${1:${i}:${#2}} == ${2} ]] && printf "%i" "$(( ${i} + 1 ))" && break
  done
  return 0
}

## USAGE:  strrchr [HAYSTACK] [NEEDLE]
## DESCRIPTION:  Finds postion of the last occurence of a substring in a string
## REQUIRES:  ---
string.strrchr() {
  declare -i i P
  [[ ! ${1} || ! ${2} ]] && return 1
  [[ ${1} != *${2}* ]] && { printf "%i" "-1"; return 1; }
  for (( i=0; i < ${#1}; i++ )); do
    [[ ${1:${i}:${#2}} == ${2} ]] && P=$(( ${i} + 1 ))
  done
  printf "%i" "${P}"
  return 0
}


## USAGE:  string.substr [STR] [START] [END]
## DESCRIPTION:  Splits [STR] based on [START] character and [END] character. Negative values work backwards. [END] is optional
## REQUIRES:  ---
string.substr() {
  declare r t x y
  [[ ! ${1} || ! ${2} || ! ${2} =~ ^[-0-9]+$ ]] || [[ ${3} && ! ${3} =~ ^[-0-9]+$ ]] && return 1
  [[ ${2:0:1} == "-" ]] && r="0" || r="1"
  [[ ${3:0:1} == "-" ]] && t="0" || t="1"
  case ${r} in
    (0)
      [[ ! ${3} ]] && { printf "%s" "${1:$(( ${#1} - ${2:1} ))}"; } || {
        case ${t} in
          (0)
            x=$(( ${#1} - ${2:1} ))
            y=$(( ( ${#1} - ${3:1} ) - ( ${#1} - ${2:1} ) ))
            (( y <= 0 )) && y=${#1}
            printf "%s" "${1:${x}:${y}}"
          ;;
          (1)
            x=$(( ${#1} - ${2:1} ))
            y=$(( ( ${#1} - ${2:1} ) - ( ${#1} - ${3} ) ))
            (( y >= 0 )) && y=${#1}
            printf "%s" "${1:${x}:${y}}"
          ;;
        esac
      }
    ;;
    (1)
      [[ ! ${3} ]] && { printf "%s" "${1:${2}}"; } || {
        case ${t} in
          (0)
            y=$(( ${2} - ( ${#1} - ${3:1} ) ))
            (( y >= 0 )) && y="-${#1}"
            printf "%s" "${1:${2}:${y:1}}"
          ;;
          (1)
            printf "%s" "${1:${2}:${3}}"
          ;;
        esac
      }
    ;;
  esac
  return 0
}

## USAGE:  string.trim [STRING]
## DESCRIPTION:  Trims whitespace from beginning and end of [STRING]
## REQUIRES:  ---
string.trim() {
  [[ ! ${1} ]] && return 1
  declare s="${1}"
  [[ ${s:0:1} =~ ^[[:space:]]$ ]] && s="${s:1}" || s="${s}"
  [[ ${s:$(( ${#s} - 1 )):1} =~ ^[[:space:]]$ ]] && s="${s:0:$(( ${#s} - 1 ))}" || s="${s}"
  printf "%s" "${s}"
  return 0
}

## USAGE:  string.upper [STRING]
## DESCRIPTION:  Convert [STRING] to uppercase
## REQUIRES:  ---
string.upper() {
  [[ ! ${1} ]] && return 1 || { printf "%s" "${1^^}"; return 0; }
}

## USAGE:  string.upper_first [STRING]
## DESCRIPTION:  Convert first character of [STRING] to uppercase
## REQUIRES:  ---
string.upper_first() {
  [[ ! ${1} ]] && return 1 || { printf "%s" "${1^}"; return 0; }
}

## USAGE:  string.upper_words [STRING]
## DESCRIPTION: Converts the first character of each word in [STRING] to uppercase
## REQUIRES:  ---
string.upper_words() {
  declare s x
  [[ ! ${1} ]] && return 1
  for x in ${1}; do
    s="${s}${x^} "
  done
  printf "%s" "${s}"
  return 0
}

## USAGE:  string.word_wrap [STRING] [CUT]
## DESCRIPTION:  Wraps a string to [CUT] characters per line; but does not break words
## REQUIRES:  ---
string.word_wrap() {
  [[ ! ${1} || ! ${2} || ! ${2} =~ ^([0-9]+)$ ]] && return 1
  declare -i j=0 i
  for (( i=0; i <= ${#1}; i++ )); do
      (( j == ${2} )) && { [[ ! ${1:${i}:1} =~ ^[[:space:]]$ ]] && { (( j-- )); } || { printf "\n"; j=0; (( i++ )); } }
    printf "%c" "${1:${i}:1}"
    (( j++ ))
  done
}
