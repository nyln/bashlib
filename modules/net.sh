##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- EXTENSION MODULE ----------
## This module is not offically supported

## USAGE:  net.ip_int
## DESCRIPTION:  Gets network cards assigned local IP address
## REQUIRES:  ip, egrep
net.ip_int() {
  [[ $(ip addr | egrep "inet" | egrep -v "127.0.0.1|::") =~ ^.+[[:space:]](.*)"/".+$ ]] && { printf "%s" "${BASH_REMATCH[1]}"; return 0; } || return 1
}

## USAGE:  net.ip_exit
## DESCRIPTION:  Gets external IP address
## REQUIRES:  wget
net.ip_ext() {
  printf "%s" $(wget "http://ifconfig.me/ip" -O - -q)
  return 0
}

## USAGE:  net.mail [TO_ADDRESS] [FROM_ADDRESS] [SUBJECT] [MESSAGE]
## DESCRIPTION:  Sends an email
## REQUIRES:  sendmail
net.mail() {
  [[ ! ${1} || ! ${2} || ! ${3} || ! ${4} ]] && return 1 || { printf "Subject: %s\nFrom: %s\nTo: %s\n\n\n%s\n" "${3}" "${2}" "${1}" "${4}" | sendmail -t; return 0; }
}
