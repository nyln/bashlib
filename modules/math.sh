##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  math.bin2dec [BINARY]
## DESCRIPTION: Converts a binary value to decimal value (integer)
## REQUIRES:  ---
math.bin2dec() {
  [[ ! ${1} || ! ${1} =~ ^[01]+$ ]] && return 1
  declare -i b="${1}" p="1" r d
  while (( b != 0 )); do
    r=$(( b % 10 ))
    d=$(( d + ( r * p ) ))
    p=$(( p * 2 ))
    b=$(( b / 10 ))
  done
  printf "%i" "${d}"
  return 0
}

## USAGE:  math.bin2hex [BINARY]
## DESCRIPTION: Converts a binary value to hex value
## REQUIRES:  ---
math.bin2hex() {
  [[ ! ${1} || ! ${1} =~ ^[01]+$ ]] && return 1 || { printf "0x%X" "${1}"; return 0; }
}

## USAGE:  math.cal [MATH]
## DESCRIPTION:  Returns answer to [MATH]
## REQUIRES:  bc
math.cal() {
  declare c
  [[ ! ${1} ]] && return 1 || { printf "%s" "$( printf "%s\n" "${@}" | bc )"; return 0; }
}

## USAGE:  math.ceil [FLOAT]
## DESCRIPTION:  Rounds [FLOAT] up to nearest integer
## REQUIRES:  ---
math.ceil() {
  [[ ! ${1} ]] && return 1
  declare IFS
  IFS="."
  declare -a c=( ${1} )
  [[ ! ${c[0]} ]] && return 1
  (( ${#c[@]} > 1 )) && printf "%i" "$(( ${c[0]} + 1 ))" || printf "%i" ${c[0]}
  return 0
}

## USAGE:  math.choose [X] [Y]
## DESCRIPTION:  Returns the binomial coefficient of [X] over [Y] 
## REQUIRES:  bc, tr

math.choose() {
  [[ ! ${1} || ! ${2} ]] && return 1
  n=$( math.fact ${1} )
  b=$( math.fact ${2} )
  x=$( math.fact $((${1}-${2})) )
  echo -e "${n}/(${b}*${x})" | bc
}

## USAGE:  math.dec2bin [INT]
## DESCRIPTION: Converts an integer to binary value
## REQUIRES:  ---
math.dec2bin() {
  [[ ! ${1} ]] && return 1
  [[ ! ${1} =~ ^[0-9]+$ ]] && return 2
        declare d="${1}" p="" b
        while (( d != 0 )); do
          p=$(( $d % 2 ))
          b="${p}${b}"
          d=$(( d / 2 ))
        done
        printf "%s" "${b}"
  return 0
}

## USAGE:  math.dec2hex [INT]
## DESCRIPTION: Converts an integer to hex value
## REQUIRES:  ---
math.dec2hex() {
  [[ ! ${1} || ! ${1} =~ ^[0-9]+$ ]] && return 1 || { printf "0x%X" "${1}"; return 0; }
}

## USAGE:  math.deg2rad [DEGREES]
## DESCRIPTION:  Converts [DEGREES] to radian
## REQUIRES:  bc
math.deg2rad() {
  [[ ! ${1} ]] && return 1
  declare r=$( printf "scale=13; ( %s * %s ) / 180\n" "${1}" "$( math.pi )" | bc )
  declare d=$( math.floor "${r}" )
  (( d < 1 )) && printf "0%s" "${r}" || printf "%s" "${r}"
  return 0
}

## USAGE:  math.fact [INT]
## DESCRIPTION: Returns the factorial of [INT]  
## REQUIRES:  bc, tr
math.fact() {
  [[ ! ${1} ]] && return 1
  n="1"
  for(( i=${1}; i>1; i-- )); do
    n="$( echo -e "${n} * ${i}" | bc | tr -d '\\' | tr -d '\n' )"
  done
  # Excessive negative check, factorials are always positive
  [[ ${n:0:1} == "-" ]] && return 2 || echo ${n}
}

## USAGE:  math.floor [FLOAT]
## DESCRIPTION:  Rounds [FLOAT] down to nearest integer
## REQUIRES:  ---
math.floor() {
  [[ ! ${1} ]] && return 1
  declare IFS
  IFS="."
  declare -a f=( ${1} )
  printf "%i" "${f[0]}"
  return 0
}

## USAGE:  math.hex2bin [HEX]
## DESCRIPTION: Converts a hex value to binary value
## REQUIRES:  ---
math.hex2bin() {
  [[ ! ${1} || ! ${1} =~ ^[[:alnum:]]+$ ]] && return 1 || { printf "%s" "$( math.dec2bin "$( math.hex2dec "${1}" )" )"; return 0; }
}

## USAGE:  math.hex2dec [HEX]
## DESCRIPTION: Converts a hex value to decimal value (integer)
## REQUIRES:  ---
math.hex2dec() {
  [[ ! ${1} || ! ${1} =~ ^[[:alnum:]]+$ ]] && return 1 || { printf "%d" "${1}"; return 0; }
}

## USAGE:  math.hypot [X] [Y]
## DESCRIPTION:  Returns length of hypotenuse of a right angle triangle
## REQUIRES:  bc
math.hypot() {
  [[ ! ${1} || ! ${2} || ! ${1} =~ ^[0-9\.]+$ || ! ${2} =~ ^[0-9\.]+$ ]] && return 2
  declare x=$( printf "${1} * ${1}\n" | bc )
  declare y=$( printf "${2} * ${2}\n" | bc )
  declare h=$( printf "${x} + ${y}\n" | bc )
  h=$( printf "sqrt ( ${h} )\n" | bc )
  printf "%s" "${h}"
  return 0
}

## USAGE:  math.is_int [INPUT]
## DESCRIPTION:  Returns true or false if [INPUT] is an integer
## REQUIRES:  ---
math.is_int() {
  [[ ! ${1} ]] && return 1
  declare s
  for s in ${@}; do
    [[ ${s} =~ ^[0-9]+$ ]] && { shift; continue; } || { printf "1"; return 1; }
  done
  printf "0"
  return 0
}

## USAGE:  math.is_float [INPUT]
## DESCRIPTION:  Returns true or false if [INPUT] is a float
## REQUIRES:  ---
math.is_float() {
  [[ ! ${1} ]] && return 1
  declare s
  for s in ${@}; do
    [[ ${s} =~ ^[0-9]+"."[0-9]+$ ]] && { shift; continue; } || { printf "1"; return "1"; }
  done
  printf "0"
  return 0
}

## USAGE:  math.max [VALUES]
## DESCRIPTION:  Returns largest value in [VALUES]
## REQUIRES:  ---
math.max() {
  [[ ! ${1} ]] && return 1
  declare c="${1}"
  shift
  declare x
  for x in ${@}; do
    declare g=$( printf "%s > %s\n" "${c}" "${1}" | bc )
    (( ${g} == 0 )) && c="${1}"
    shift
  done
  printf "%s" "${c}"
  return 0
}

## USAGE:  math.min [VALUES]
## DESCRIPTION:  Returns smallest value in [VALUES]
## REQUIRES:  ---
math.min() {
  [[ ! ${1} ]] && return 1
  declare c="${1}"
  shift
  declare x
  for x in ${@}; do
    declare l=$( printf "%s < %s\n" "${c}" "${1}" | bc )
    (( ${l} == 0 )) && c="${1}"
    shift
  done
  printf "%s" "${c}"
  return 0
}

## USAGE:  math.pi
## DESCRIPTION:  Returns value of pi up to 13 decimal places
## REQUIRES:  ---
math.pi() {
  printf "3.1415926535898"
  return 0
}

## USAGE:  math.deg2rad [DEGREES]
## DESCRIPTION:  Converts [DEGREES] to radian
## REQUIRES:  bc
math.rad2deg() {
  [[ ! ${1} ]] && return 1 || { printf "%0.2f" "$( printf "scale=12; %s * 180 / %s\n" "${1}" "$( math.pi )" | bc )"; return 0; }
}

## USAGE:  math.rand [X] [Y]
## DESCRIPTION:  Generates a pseudorandom integer between [X] and [Y]
## REQUIRES:  ---
math.rand() {
  [[ ! ${1} || ! ${2} ]] && return 1 || { printf "%i" "$(( ${1} + ( ${RANDOM} % ( ( ${2} - ${1} ) + 1 ) ) ))"; return 0; }
}

## USAGE:  math.round [FLOAT]
## DESCRIPTION:  Rounds [FLOAT] to nearest integer
## REQUIRES:  ---
math.round() {
        [[ ! ${1} ]] && return 1
  declare IFS
  IFS="."
  declare -a r=( ${1} )
  (( ${r[1]:0:1} < 5 )) && printf "%i" ${r[0]} || printf "%i" $(( ${r[0]} + 1 ))
  return 0
}
