##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- EXTENSION MODULE ----------
## This module is not offically supported

## USAGE:       conversion.area_intomm [VALUE]
## DESCRIPTION: Converts [VALUE] inches squared to milimeters squared
## REQUIRES:  bc
conversion.area_intomm() {
  printf "%s" "$( echo "${1} / 0.00155" | bc )"
}

## USAGE:       conversion.area_mmtoin [VALUE]
## DESCRIPTION: Converts [VALUE] milimeters squared to inches squared
## REQUIRES:  bc
conversion.area_mmtoin() {
  printf "%s" "$( echo "${1} * 0.00155" | bc )"
}

## USAGE:       conversion.length_intomm_[VALUE]
## DESCRIPTION: Converts [VALUE] inches to milimeters
## REQUIRES:  bc
conversion.length_intomm() {
  printf "%s" "$( echo "${1} / 0.03937" | bc )"
}

## USAGE:       conversion.length_mmtoin [VALUE]
## DESCRIPTION: Converts [VALUE] milimeters to inches
## REQUIRES:  bc
conversion.length_mmtoin() {
  printf "%s" "$( echo "${1} * 0.03937" | bc )"
}

## USAGE:       conversion.temperature_[A]to[B] [VALUE]
## DESCRIPTION: Converts [VALUE] temperature [A] to [B] (celcius, fahrenheit, kelvin, rankine, newton, delisle)
## REQUIRES:  bc
conversion.temperature_ctode() {
  printf "%s" "$( echo "( 100 - ${1} ) * 1.5" | bc )"
}

conversion.temperature_ctof() {
  printf "%s" "$( echo "${1} * 1.8 + 32" | bc )"
}

conversion.temperature_ctok() {
  printf "%s" "$( echo "${1} + 273.15" | bc )"
}

conversion.temperature_cton() {
  printf "%s" "$( echo "${1} * 0.33" | bc )"
}

conversion.temperature_ctor() {
  printf "%s" "$( echo "( ${1} * 1.8 + 32 ) + 459.67" | bc )"
}

conversion.temperature_ftoc() {
  printf "%s" "$( echo "( ${1} - 32 ) / 1.8" | bc )"
}

conversion.temperature_ftode() {
  printf "%s" "$( echo "( ${1} - 32 ) * 0.83333 - 100" | bc )"
}

conversion.temperature_ftok() {
  printf "%s" "$( echo "( ( ${1} - 32 ) / 1.8 ) + 273.15" | bc )"
}

conversion.temperature_fton() {
  printf "%s" "$( echo "( ${1} - 32 ) * 0.18333" | bc )"
}

conversion.temperature_ftor() {
  printf "%s" "$( echo "${1} + 459.67" | bc )"
}

conversion.temperature_detoc() {
  printf "%s" "$( echo "${1} + 100 / 1.5" | bc )"
}

conversion.temperature_detof() {
  printf "%s" "$( echo "( ${1} - 32 ) * 0.83333 - 100" | bc )"
}

conversion.temperature_detok() {
  printf "%s" "$( echo "( ( ${1} + 100 ) / 1.5 ) + 273.15" | bc )"
}

conversion.temperature_deton() {
  printf "%s" "$( echo "( ${1} + 100 ) * 0.22" | bc )"
}

conversion.temperature_detor() {
  printf "%s" "$( echo "( ${1} + 100 ) * 1.2 + 491.67" | bc )"
}

conversion.temperature_ktoc() {
  printf "%s" "$( echo "${1} - 273.15" | bc )"
}

conversion.temperature_ktode() {
  printf "%s" "$( echo "( ${1} - 273.15 ) * 1.5 - 100" | bc )"
}

conversion.temperature_ktof() {
  printf "%s" "$( echo "( ${1} - 273.15 ) * 1.8 + 32" | bc )"
}

conversion.temperature_kton() {
  printf "%s" "$( echo "( ${1} - 273.15 ) * 0.33" | bc )"
}

conversion.temperature_ktor() {
  printf "%s" "$( echo "( ${1} - 273.15 ) * 1.8 + 491.67" | bc )"
}

conversion.temperature_ntoc() {
  printf "%s" "$( echo "${1} / 0.33" | bc )"
}

conversion.temperature_ntode() {
  printf "%s" "$( echo "${1} * 4.5455 - 100" | bc )"
}

conversion.temperature_ntof() {
  printf "%s" "$( echo "${1} * 5.4545 + 32" | bc )"
}

conversion.temperature_ntok() {
  printf "%s" "$( echo "${1} / 0.33 + 273.15" | bc )"
}

conversion.temperature_ntor() {
  printf "%s" "$( echo "${1} * 5.4545 + 491.67" | bc )"
}

conversion.temperature_rtoc() {
  printf "%s" "$( echo "(${1} - 491.67 ) / 1.8" | bc )"
}

conversion.temperature_rtode() {
  printf "%s" "$( echo "( ${1} - 491.67 ) * 0.83333 - 100" | bc )"
}

conversion.temperature_rtof() {
  printf "%s" "$( echo "( ${1} - 491.67 ) + 32" | bc )"
}

conversion.temperature_rtok() {
  printf "%s" "$( echo "( ${1} - 491.67 ) / 1.8 + 273.15" | bc )"
}

conversion.temperature_rton() {
  printf "%s" "$( echo "( ${1} - 491.67 ) * 0.18333" | bc )"
}

## USAGE:       conversion.weight_[A]to[B] [VALUE]
## DESCRIPTION: Converts [VALUE] weight [A] to [B] (carret, ounce, grams)
## REQUIRES:  bc
conversion.weight_cttog() {
  printf "%s" "$( echo "${1} / 5.0" | bc )"
}

conversion.weight_cttoz() {
  printf "%s" "$( echo "${1} * 0.0070548" | bc )"
}

conversion.weight_gtoct() {
  printf "%s" "$( echo "${1} * 5.0" | bc )"
}

conversion.weight_gtooz() {
  printf "%s" "$( echo "${1} * 0.035274" | bc )"
}

conversion.weight_oztoct() {
  printf "%s" "$( echo "${1} * 141.75" | bc )"
}

conversion.weight_oztog() {
  printf "%s" "$( echo "${1} / 0.035274" | bc )"
}
