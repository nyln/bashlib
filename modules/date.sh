##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  date.diff [DATE_A] [DATE_B]
## DESCRIPTION:  Gets the number of seconds difference between [DATE_A] and [DATE_B]
## REQUIRES:  date
date.diff() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i a=$( date --date="${1}" +%s )
  declare -i b=$( date --date="${2}" +%s )
  (( a > b )) && declare -i d=$(( a - b )) || declare -i d=$(( b - a ))
  printf "%i" "${d}"
  return 0
}

## USAGE:  date.days [SECONDS]
## DESCRIPTION:  Converts [SECONDS] to days
## REQUIRES:  bc
date.days() {
  [[ ! ${1} ]] && return 1
  printf "%i" "$( echo "${1} / 60 / 60 / 24" | bc )"
  return 0
}

## USAGE:   date.get_date
## DESCRIPTION:  Gets current date in YYYY/MM/DD format
## REQUIRES:  date
date.get_date() {
  printf "%s" "$( date "+%Y/%m/%d" )"
  return 0
}

## USAGE:  date.get_date_time
## DESCRIPTION:  Gets current date and time in YYYY/MM/DD HH:MM:SS format
## REQUIRES:  date
date.get_date_time() {
  printf "%s" "$( date "+%Y/%m/%d %H:%M:%S" )"
  return 0
}

## USAGE:  date.get_seconds
## DESCRIPTION:  Get current date and time in seconds (since EPOC)
## REQUIRES:  date
date.get_seconds() {
  printf "%s" "$( date "+%s" )"
  return 0
}

## USAGE:  date.get_time
## DESCRIPTION:  Gets current time in HH:MM:SS format
## REQUIRES:  date
date.get_time() {
  printf "%s" "$( date "+%H:%M:%S" )"
  return 0
}

## USAGE:  date.is_leap_year [DATE]
## DESCRIPTION:  Returns true or false if [DATE] is in a leap year
## REQUIRE:  bc
date.is_leap_year() {
  [[ ! ${1} ]] && return 1
  declare y=$( echo "scale=1; $( date +%y --date="${1}" ) / 4" | bc )
  (( ${y:$(( ${#y} - 1 )):1} == 0 )) && printf "0" && return 0 || printf "1"
  return 1
}

## USAGE:  date.months [SECONDS]
## DESCRIPTION:  Converts [SECONDS] to months
## REQUIRES:  bc
date.months() {
  [[ ! ${1} ]] && return 1
  printf "%i" "$( echo "${1} / 60 / 60 / 24 / 30" | bc )"
  return 0
}

## USAGE:  date.seconds [DATE]
## DESCRIPTION:  Converts [DATE] to seconds
## REQUIRES:  date
date.seconds() {
  [[ ! ${1} ]] && return 1
  printf "%i" $( date --date="${1}" +%s )
  return 0
}

## USAGE:  date.weeks [SECONDS]
## DESCRIPTION:  Converts [SECONDS] to weeks
## REQUIRES:  bc
date.weeks() {
  [[ ! ${1} ]] && return 1
  printf "%i" "$( echo "${1} / 60 / 60 / 60 / 24 / 7" | bc )"
  return 0
}

## USAGE:  date.years [SECONDS]
## DESCRIPTION:  Converts [SECONDS] to years
## REQUIRES:  bc
date.years() {
  [[ ! ${1} ]] && return 1
  printf "%i" "$( echo "${1} / 60 / 60 / 24 / 7 / 52" | bc )"
  return 0
}
