##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  database.mysql_create [DB_NAME]
## DESCRIPTION: Creates a new MySQL database called [DB_NAME]
## REQUIRES:  mysql
database.mysql_create() {
  [[ ! ${1} ]] && return 1
  declare q="CREATE DATABASE IF NOT EXISTS ${1};"
  mysql -s -e "${q}" &>/dev/null
  (( ${?} != 0 )) && return 1 || return 0
}

## USAGE:  database.mysql_delete [DB_NAME]
## DESCRIPTION:  Deletes the database [DB_NAME]
## REQUIRES:  mysql
database.mysql_delete() {
  [[ ! ${1} ]] && return 1
  declare q="DROP DATABASE IF EXISTS ${1};"
  mysql -s -e "${q}" &>/dev/null
  (( ${?} != 0 )) && return 1 || return 0
}

## USAGE:  database.mysql_query [DB_NAME] [QUERY]
## DESCRIPTION:  Returns results of [QUERY] on database [DB_NAME]
## REQUIRES:  mysql
database.mysql_query() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare i
  declare q="USE ${1}; ${2}"
  declare e=$( mysql -s -e "${q}" | tail -n +1 )
  (( ${?} != 0 )) && return 1
  printf "%s\n" "${e}" |
  while read line; do
    for i in ${line}; do
      printf "%s;" "${i}"
    done
    printf "\n"
  done
  return 0
}

## USAGE:  database.mysql_user_assign [USER_NAME] [DB_NAME]
## DESCRIPTION:  Assigns user [USER_NAME] to database [DB_NAME]
## REQUIRES:  mysql
database.mysql_user_assign() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare q="GRANT ALL PRIVILEDGES ON ${2}.* TO ${1}@localhost; FLUSH PRIVILEGES;"
  mysql -s -e "${q}" &>/dev/null
  (( ${?} != 0 )) && return 1 || return 0
}

## USAGE:  database.mysql_user_create [USER_NAME] [PASSWORD]
## DESCRIPTION:  Creates a new MySQL user called [USER_NAME] with [PASSWORD]
## REQUIRES:  mysql
database.mysql_user_create() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare q="CREATE USER ${1}@localhost IDENTIFIED BY '${2}';"
  mysql -s -e "${q}" &>/dev/null
  (( ${?} != 0 )) && return 1 || return 0
}

## USAGE:  database.mysql_user_delete [USER_NAME]
## DESCRIPTION:  Deletes the user [USER_NAME]
## REQUIRES:  mysql
database.mysql_user_delete() {
  [[ ! ${1} ]] && return 1
  declare q="DROP USER ${1}@localhost; FLUSH PRIVILEGES;"
  mysql -s -e "${q}" &>/dev/null
  (( ${?} != 0 )) && return 1 || return 0
}

## USAGE:  database.sqlite_query [DB_NAME] [QUERY]
## DESCRIPTION:  Executs [QUERY] on sqlite database [DB_NAME]
## REQUIRES:  sqlite3
database.sqlite_query() {
  [[ ! ${1} || ! ${2} ]] && return 1
  sqlite3 "${1}" "${2}"
  return 0
}
