##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- EXTENSION MODULE ----------
## This module is not offically supported

# USAGE:  system.battery_status
## DESCRIPTION:  Returns percentage of battery remaining
## REQUIRES:  acpi
system.battery_status() {
  declare -a b=( $( acpi ) )
  printf "%i" ${b[3]}
  return 0
}

## USAGE:  system.cpu_usage
## DESCRIPTION:  Returns percentage of CPU being used
## REQUIRES:  awk
system.cpu_usage() {
  printf "%s" "$( ps -eo pcpu | egrep -v "0.0|CPU" | awk '{sum+=$1}END{print sum}' )"
  return 0
}

## USAGE:  system.dep [BINARY]
## DESCRIPTION:  Returns true or false if [BINARY] is a usable binary / builtin
## REQUIRES:  ---
system.dep() {
  [[ ! ${1} ]] && return 1
  declare -a s=( ${@} )
  declare b e
  for b in ${s[@]}; do
    e=$( type -t ${b} )
    [[ ! ${e} ]] && printf "1" || printf "0"
  done
  return 0
}

## USAGE:  system.disk_usage [DISK]
## DESCRIPTION:  Returns percentage of all disks being used -- if [DISK] is set returns single [DISK] usage
## REQUIRES:  df
system.disk_usage() {
  declare d=$( df -h --output=source,pcent | grep "/dev/" | awk '{sum+=$2}END{print sum}' )
  [[ ${1} ]] && d=$( df -h --output=source,pcent | grep "${1}" | awk '{sum+=$2}END{print sum}' )
  printf "%s" "${d}"
  return 0
}

## USAGE:  system.is_root
## DESCRIPTION:  Returns true or false if user has root permissions
## REQUIRES:  ---
system.is_root() {
  [[ ${EUID} ]] && { (( EUID == 0 )) && printf "0" || printf "1"; } || { (( $( id -u ) == 0 )) && printf "0" || printf "1"; }
  return 0
}

## USAGE:  system.mem_usage
## DESCRIPTION:  Returns percentage of memory being used
## REQUIRES:  bc
system.mem_usage() {
  t=$( echo $( cat /proc/meminfo | grep "MemTotal" | awk '{print $2}' ) / 1024 | bc )
  f=$( echo $( cat /proc/meminfo | grep "MemFree" | awk '{print $2}') / 1024 | bc )
  u=$(( t - f ))
  p=$( echo "scale=3; ${u} / ${t}" | bc | cut -d "." -f 2 )
  d=${p:$(( ${#p} - 1 )):1}
  p=${p:0:$(( ${#p} - 1 ))}
  printf "%i.%i" ${p} ${d}
  return 0
}

## USAGE:  system.pid_running [PID]
## DESCRIPTION:  Checks if [PID] is running
## REQUIRES:  ps, egrep
system.pid_running() {
  [[ ! ${1} ]] && return 1
  (( $( ps xco pid | egrep -c "^${1}$" ) == 0 )) && { printf "1" && return 1; } || { printf "0"; return 0; }
}

## USAGE:  system.process_running [NAME]
## DESCRIPTION:  Checks if process [NAME] is running
## REQUIRES:  ps, egrep
system.process_running() {
  [[ ! ${1} ]] && return 1
  (( $( ps xco command | egrep -c "^${1}$" ) == 0 )) && { printf "1"; return 1; } || { printf "0"; return 0; }
}
