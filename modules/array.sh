##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  array.amend [ARRAY_NAME] [INDEX] [VALUE]
## DESCRIPTION:  Modifieds [ARRAY_NAME][INDEX] to [VALUE]
## REQUIRES:  ---
array.amend() {
  [[ ! ${1} || ! ${2} ]] && return 1 || { eval "${1}[${2}]=\"${@:3}\""; return 0; }
}

## USAGE:  array.append [ARRAY_NAME] [VALUES]
## DESCRIPTION:  Append [VALUES] to [ARRAY_NAME]
## REQUIRES:  ---
array.append() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=1; i < ${#@}; i++ )); do
    eval "${1}[\$(( i + ${#1} - 1 ))]=\"\${@:\$(( i + 1 )):1}\""
  done
  return 0
}

## USAGE:  array.concate [NEW_ARRAY_NAME] [OLD_ARRAY_NAME_A] [OLD_ARRAY_NAME_B]
## DESCRIPTION:  Appends [OLD_ARRAY_NAME_B] to [OLD_ARRAY_NAME_A] into [NEW_ARRAY_NAME]
## REQUIRES:  ---            
array.concate() {
  [[ ! ${1} || ! ${2} || ! ${3} ]] && return 1
  declare -i i j
  for (( i=0; i < $( eval "printf \"%i\" \"\${#${2}[@]}\"" ); i++ )); do
    eval "${1}[${i}]=\"\${${2}[\${i}]}\""
  done
  for (( j=0; j < $( eval "printf \"%i\" \"\${#${3}[@]}\"" ); j++ )); do
    eval "${1}[$(( j + i ))]=\"\${${3}[\${j}]}\""
  done
  return 0
}

## USAGE:  array.copy [NEW_ARRAY] [OLD_ARRAY]
## DESCRIPTION:  Duplicates contents of [OLD_ARRAY] into [NEW_ARRAY]
## REQUIRES:  ---
array.copy() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=0; i < $( eval "printf \"%i\" \"\${#${2}[@]}\"" ); i++ )); do
    eval "${1}[${i}]=\"\${${2}[\${i}]}\""
  done
  return 0
}

## USAGE:  array.create [ARRAY_NAME] [VALUES]
## DESCRIPTION:  Creates [ARRAY_NAME] with starting values of [VALUES]
## REQUIRES:  ---
array.create() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=0; i < $(( ${#@} - 1 )); i++ )); do
    eval "${1}[\${i}]=\"\${@:\$(( i + 2 )):1}\""
  done
  return 0
}

## USAGE:  array.destroy [ARRAY_NAME]
## DESCRIPTION:  Completely destroys [ARRAY_NAME]
## REQUIRES:  ---
array.destroy() {
  [[ ! ${1} ]] && return 1 || { unset "${1}"; return 0; }
}

## USAGE:  array.find [ARRAY_NAME] [VALUE]
## DESCRIPTION:  Checks if [VALUE] exists in [ARRAY_NAME] and return array items
## REQUIRES:  ---
array.find() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=0; i < $( eval "printf \${#${1}[@]}" ); i++ )) do
    [[ $( eval printf \"\${${1}[\${i}]}\" ) == "${2}" ]] && printf "%i " "${i}"
  done
  return 0
}

## USAGE:  array.find_ilike [ARRAY_NAME] [SEARCH]
## DESCRIPTION:  Checks if a value (case insensitive) LIKE %[SEARCH]% exists in [ARRAY_NAME] and return array items
## REQUIRES:  ---
array.find_ilike() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=0; i < $( eval "printf \${#${1}[@]}" ); i++ )) do
    [[ $( eval printf \"\${${1}[\${i}]\,\,}\" ) =~ ^(.*)"${2,,}"(.*)$ ]] && printf "%i " "${i}"
  done
  return 0
}

## USAGE:  array.find_like [ARRAY_NAME] [SEARCH]
## DESCRIPTION:  Checks if a value LIKE %[SEARCH]% exists in [ARRAY_NAME] and return array items
## REQUIRES:  ---
array.find_like() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=0; i < $( eval "printf \${#${1}[@]}" ); i++ )) do
    [[ $( eval printf \"\${${1}[\${i}]}\" ) =~ ^(.*)"${2}"(.*)$ ]] && printf "%i " "${i}"
  done
  return 0
}

## USAGE:  array.ifind [ARRAY_NAME] [VALUE]
## DESCRIPTION:  Checks if [VALUE] exists in [ARRAY_NAME] and return array items, case insensitive
## REQUIRES:  ---
array.ifind() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=0; i < $( eval "printf \${#${1}[@]}" ); i++ )) do
    [[ $( eval printf \"\${${1}[\${i}]\,\,}\" ) == "${2,,}" ]] && printf "%i " "${i}"
  done
  return 0
}


## USAGE:  array.len [ARRAY_NAME]
## DESCRIPTION:  Returns number of keys in [ARRAY_NAME]
## REQUIRES:  ---
array.len() {
  [[ ! ${1} ]] && return 1 || { printf "%i" $( eval echo "\${#${1}[@]}" ); return 0; }
}

## USAGE:  array.list [ARRAY_NAME]
## DESCRIPTION:  Returns contents of [ARRAY_NAME]
## REQUIRES:  ---
array.list() {
  [[ ! ${1} ]] && return 1
  declare -i i
  for (( i=0; i < $( eval printf "%i " "\${#${1}[@]}" ); i++ )); do
    eval "printf \"%s[%i]=%s\n\" \"${1}\" \"${i}\" \"\${${1}[${i}]}\""
  done
  return 0
}

## USAGE:  array.prepend [ARRAY_NAME] [VALUES]
## DESCRIPTION:  Prepend [VALUES] to [ARRAY_NAME]
## REQUIRES:  ---
array.prepend() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare -i i
  for (( i=$( eval "printf \"%i\" \"\${#${1}[@]}\"" ); i >= 0; i-- )); do
    eval "[[ \${${1}[\${i}]} ]] && ${1}[\$(( i + \${#@} - 1 ))]=\${${1}[\${i}]}"
  done
  for (( i=0; i < $(( ${#@} - 1 )); i++ )); do
    eval "${1}[\${i}]=\"\${@:\$(( i + 2 )):1}\""
  done
  return 0
#  { eval "${1}=( ${@:2} \${${1}[@]} )"; return 0; }
}

## USAGE:  array.sort [ARRAY_NAME]
## DESCRIPTION:  Sorts [ARRAY_NAME] in alpha numerical order
## REQUIRES:  fmt, sort
array.sort() {
  [[ ! ${1} ]] && return 1
  declare -i i
  declare IFS
  for (( i=0; i < $( eval "printf \"%i\" \"\${#${1}[@]}\"" ); i++ )); do
    declare S="${S}\""$( eval "printf \"%s\" \"\${${1}[\${i}]}\"" )"\"\n"
  done
  IFS="
  "
  S=( $( printf "%b" "${S}" | sort -di ) )
  for (( i=0; i < ${#S[@]}; i++ )); do
    eval "${1}[\${i}]=\"\${S[\${i}]}\""
  done
}

## USAGE:  array.sort_reverse [ARRAY_NAME]
## DESCRIPTION:  Sorts [ARRAY_NAME] in reverse alpha numerical order
## REQUIRES:  fmt, sort
array.sort_reverse() {
  [[ ! ${1} ]] && return 1
  declare -i i
  declare IFS
  for (( i=0; i < $( eval "printf \"%i\" \"\${#${1}[@]}\"" ); i++ )); do
    declare S="${S}\""$( eval "printf \"%s\" \"\${${1}[\${i}]}\"" )"\"\n"
  done
  IFS="
  "
  S=( $( printf "%b" "${S}" | sort -dir ) )
  for (( i=0; i < ${#S[@]}; i++ )); do
    eval "${1}[\${i}]=\"\${S[\${i}]}\""
  done
}
