##
##  Copyright (c) 2014-2022 Maisy Donachie
##
##  Licensed under the Apache License, Version 2.0 (the "License");
##  you may not use this file except in compliance with the License.
##  You may obtain a copy of the License at
##
##  http://www.apache.org/licenses/LICENSE-2.0
##
##  Unless required by applicable law or agreed to in writing, software
##  distributed under the License is distributed on an "AS IS" BASIS,
##  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
##  See the License for the specific language governing permissions and
##  limitations under the License.
##

## ---------- CORE MODULE -----------
## This module is offically supported

## USAGE:  file.encrypt_aes [FILE]
## DESCRIPTION:  Encrypt [FILE] with an AES256 key
## REQUIRES:  openssl
file.encrypt_aes() {
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  openssl enc -aes256 -in "${1}" -out "${1}.aes256"
  return 0
}

## USAGE:  file.decrypt_aes [FILE]
## DESCRIPTION:  Decrypt [FILE] with AES256 key
## REQUIRES:  openssl
file.decrypt_aes() {
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  openssl enc -d -aes256 -in "${1}.aes256" -out "${1}"
  return 0
}

## USAGE:  file.cat [FILE]
## DESCRIPTION:  Print out contents of [FILE]
## REQUIRES:  ---
file.cat() {
  declare REPLY
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  while read -r; do
    printf "%s\n" "${REPLY}"
  done < "${1}"
  return 0
}

## USAGE:  file.dechaff [FILE]
## DESCRIPTION:  Creates [FILE].dechaff removing whitespace and comment lines
## REQUIRES:  cat, egrep
file.dechaff() {
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  cat ${1} | egrep -i -v "^#[^\!]|^$" > ${1}.dechaff
  return 0
}

## USAGE:  file.diff [FILE_A] [FILE_B]
## DESCRIPTION:  Returns the number of different lines between [FILE_A] and [FILE_B]
## REQUIRES:  diff, wc
file.diff() {
  [[ ! ${1} || ! ${2} || ! -f "${1}" || ! -f "${2}" ]] && return 1
  declare -i d=$( diff ${1} ${2} | wc -l )
  (( d == 0 )) && printf "0" || printf "%i" $(( d / 2 ))
  return 0
}

## USAGE:  file.lines [FILE]
## DESCRIPTION:  Returns numbers of lines in a given file
## REQUIRES:  grep
file.lines() {
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  printf "%i" $( grep -c "" "${1}" )
  return 0
}

## USAGE:  file.md5 [FILE/DIRECTORY]
## DESCRIPTION:  Get an md5 hash of a single file or a directory (recursivly)
## REQUIRES:  md5sum, find, xargs
file.md5() {
  [[ ! ${1} ]] && return 1
  declare TYPE
  [[ -f "${1}" ]] && md5sum "${1}" || printf "%s\n" "$( find "${1}" -type f | sort | xargs md5sum | md5sum )"
  return 0
}

## USAGE:  file.parse_cfg [FILE]
## DESCRIPTION:  Returns values of [FILE], read as a cfg file as (VARIABLE VALUE VARIABLE VALUE)...
## REQUIRES:  ---
file.parse_cfg() {
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  declare -i i=0
  while read line; do
    [[ ! ${line} =~ ^(.*)[=](.*)$ ]] && {
      (( i > 0 )) && printf " "
      printf "ERROR %s" "${line}"
      return 3
    } || {
      (( i > 0 )) && printf " "
      printf "%s %s" "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}"
      (( i++ ))
    }
  done < "${1}"
  return 0
}

## USAGE:  file.parse_xml [FILE]
## DESCRIPTION:  Returns values of [FILE], read as an xml file as (VARIABLE VALUE VARIABLE VALUE)...
## REQUIRES:  ---
file.parse_xml() {
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  declare -i i=0
  while read line; do
    [[ ! ${line} =~ ^["<"](.*)[">"](.*)["<"]["/"](.*)[">"](.*)$ ]] && {
      (( i > 0 )) && printf " "
      printf "ERROR %s" "${line}"
      return 3
    } || {
      (( i > 0 )) && printf " "
      printf "%s %s" "${BASH_REMATCH[1]}" "${BASH_REMATCH[2]}"
      (( i++ ))
    }
  done < "${1}"
  return 0
}

## USAGE:  file.perm_check [FILE/DIRECTORY] [PERMISSIONS]
## DESCRIPTION:  Check permissions of a file or directory against what we are expecting
## REQUIRES:  find, wc
file.perm_check() {
  [[ ! ${1} || ! ${2} || ! -f "${1}" && ! -d "${1}" ]] && return 1
  declare -i f=$( find "${1}" -perm ${2} | wc -l )
  (( f == 1 )) && printf "0" || printf "1"
  return 0
}

## USAGE:  file.perm_get [FILE/DIRECTORY]
## DESCRIPTION:  Returns the current permissions of the requested file / directory
## REQUIRES:  stat
file.perm_get() {
  [[ ! ${1} || ! -f "${1}" && ! -d "${1}" ]] && return 1
  printf "%i" "$( stat "${1}" -c %a )"
  return 0
}

## USAGE:  file.perm_set [FILE/DIRECTORY] [PERMISSIONS]
## DESCRIPTION:  Sets permissions of a file or directory
## REQUIRES:  find, chmod, wc
file.perm_set() {
  [[ ! ${1} || ! ${2} || ! -f "${1}" || ! -d "${1}" ]] && return 1
  find "${1}" -exec chmod "${2}" {} \;
  (( $( file.perm_check "${1}" "${2}" "${3}" ) == 0 )) && printf "0" || printf "1"
  return 0
}

## USAGE:  file.remove [FILE/DIRECTORY]
## DESCRIPTION:  Deletes [FILE/DIRECTORY] (directory requires a trailing /)
## REQUIRES:  rm
file.remove() {
  [[ ! ${1} || ! -f "${1}" && ! -d "${1}" ]] && return 1
  [[ "${1:$(( ${#1} - 1 )):1}" == "/" ]] && rm -rf "${1}" || rm -f "${1}"
  return 0
}

## USAGE:  file.sha1 [FILE/DIRECTORY]
## DESCRIPTION:  Get an sha1 hash of a single file or a directory (recursivly)
## REQUIRES:  sha1sum, find, xargs
file.sha1() {
  [[ ! ${1} ]] && return 1
  declare TYPE
  [[ -f "${1}" ]] && sha1sum "${1}" || printf "%s\n" "$( find "${1}" -type f | sort | xargs sha1sum | sha1sum )"
  return 0
}

## USAGE:  file.sock [FILE]
## DESCRIPTION:  Watches [FILE] for changes and then executes it (based on the #! line)
## REQUIRES:  grep
## ---- THIS FUNCTION IS A PROTOTYPE AND SHOULD NOT BE RELIED ON! ----
file.sock() {
  declare L A H
  [[ ! ${1} || ! -f "${1}" ]] && return 1
  L=$( stat -c %Z "${1}" )
  while ( true ); do
    A=$( stat -c %Z "${1}" )
    [[ "${A}" != "${L}" ]] && {
      H=$( grep "#!" "${1}" )
      [[ ${H} =~ ^"#!"(.*)$ ]] && ${BASH_REMATCH[1]} "${1}" || bash "${1}"
      L=${A}
      printf "\n" > "${1}"
    }
    sleep 0.1
  done
  return 0
}

## USAGE:  file.tar_gz_pack [ARCHIVE_NAME] [LOCATION]
## DESCRIPTION:  Packs [LOCATION] into [ARCHIVE_NAME].tar.gz
## REQUIRES:  tar
file.tar_gz_pack() {
  [[ ! ${1} || ! ${2} ]] && return 1
  declare x
  declare a="${1}"
  shift
  declare -a f=( ${@} )
  for x in ${f[@]}; do
    [[ ! -f "${x}" && ! -d "${x}" ]] && return 2
  done
  tar acf "${a}.tar.gz" "${f[@]}" &>/dev/null
  (( ${?} == 0 )) && return "0" || return ${?}
}

## USAGE:  file.tar_gz_unpack [ARCHIVE_NAME] [FILENAME]
## DESCRIPTION:  Unpacks [FILENAME] from [ARCHIVE_NAME].tar.gz to current directory
## REQUIRES:  tar
file.tar_gz_unpack() {
  [[ ! ${1} || ! ${2} || ! -f "${1}" ]] && return 1
  declare a="${1}"
  shift
  delcare -a f
  f=( ${@} )
  tar xvf "${a}.tar.gz" "${f[@]}" &>/dev/null
  (( ${?} == 0 )) && return 0 || return ${?}
}

## USAGE:  file.touch [FILE/DIRECTORY]
## DESCRIPTION:  Creates [FILE/DIRECTORY] (directory requires a trailing /)
## REQUIRES:  mkdir, touch
file.touch() {
  [[ ! ${1} || -f "${1}" || -d "${1}" ]] && return 1
  [[ "${1:$(( ${#1} - 1 )):1}" == "/" ]] && mkdir "${1}" || touch "${1}"
  return 0
}
